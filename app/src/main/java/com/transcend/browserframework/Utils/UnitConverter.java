package com.transcend.browserframework.Utils;

import android.content.Context;
import android.util.DisplayMetrics;

import com.transcend.browserframework.R;

import java.lang.ref.WeakReference;

/**
 * Created by mike_chen on 2017/12/26.
 */

public class UnitConverter {

    private static WeakReference<Context> mContext;

    public UnitConverter(Context context){
        mContext = new WeakReference<>(context);
    }

    /**
     * Covert px to dp
     * @param px
     * @return dp
     */
    public static float convertPixelToDp(float px){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return px;

        float dp = (px*160/320) * getDensity(context);    //paw給的為320dpi，故px/2就等於我們要的dp，在乘上密度則完整
        return (float) (dp);  //微調數據
    }

    /**
     * Covert pt to sp
     * @param pt
     * @return sp
     */
    public static float convertPtToSp(float pt){
        float sp = (pt/100 *45) ;
        return (float) (sp);    //微調數據
    }
    /**
     * 取得螢幕密度
     * 120dpi = 0.75
     * 160dpi = 1 (default)
     * 240dpi = 1.5
     * @param context
     * @return
     */
    public static float getDensity(Context context){
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return metrics.density;
    }

    public String parseDate(String date){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return date;

        if(date.length() != "20171228".length())    //20171221 開會討論的日期格式，不符合格式則回傳原字串
            return date;

        String year = date.substring(0,4);
        String month = date.substring(4,6);
        String day = date.substring(6,8);

        switch (month){
            case "01":
                return context.getResources().getString(R.string.framework_january)+" "+day+", "+year;
            case "02":
                return context.getResources().getString(R.string.framework_february)+" "+day+", "+year;
            case "03":
                return context.getResources().getString(R.string.framework_march)+" "+day+", "+year;
            case "04":
                return context.getResources().getString(R.string.framework_april)+" "+day+", "+year;
            case "05":
                return context.getResources().getString(R.string.framework_may)+" "+day+", "+year;
            case "06":
                return context.getResources().getString(R.string.framework_june)+" "+day+", "+year;
            case "07":
                return context.getResources().getString(R.string.framework_july)+" "+day+", "+year;
            case "08":
                return context.getResources().getString(R.string.framework_august)+" "+day+", "+year;
            case "09":
                return context.getResources().getString(R.string.framework_september)+" "+day+", "+year;
            case "10":
                return context.getResources().getString(R.string.framework_october)+" "+day+", "+year;
            case "11":
                return context.getResources().getString(R.string.framework_november)+" "+day+", "+year;
            case "12":
                return context.getResources().getString(R.string.framework_december)+" "+day+", "+year;
        }
        return "";
    }
}
