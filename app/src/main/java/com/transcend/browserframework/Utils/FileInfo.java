package com.transcend.browserframework.Utils;

import android.net.Uri;
import android.os.Parcel;
import android.os.Parcelable;

import com.transcend.browserframework.R;

/**
 * Created by mike_chen on 2018/1/16.
 */

public class FileInfo implements Parcelable {

    public String groupTitle;
    public String title;
    public String subtitle;
    public int type;
    public String path;
    public long size;
    public int mediaIconResourceId = -1;
    public int defaultIconResourceId = -1;

    public String uri;
    public int storage;

    public boolean showTitleLayout;
    public boolean isChecked;

    public static int mediaTypeTitleWithoutIcon = 0;

    public FileInfo(){
        showTitleLayout = true; //default to show title
        isChecked = false;  //default not be checked
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(groupTitle);
        dest.writeString(title);
        dest.writeString(subtitle);
        dest.writeInt(type);
        dest.writeString(path);
        dest.writeLong(size);
        dest.writeInt(showTitleLayout ? 1: 0);
        dest.writeInt(isChecked ? 1 : 0);
        dest.writeInt(mediaIconResourceId);
        dest.writeInt(defaultIconResourceId);
        dest.writeString(uri);
        dest.writeInt(storage);
    }

    public static Parcelable.Creator<FileInfo> CREATOR = new Creator<FileInfo>() {
        @Override
        public FileInfo createFromParcel(Parcel source) {
            FileInfo fileInfo = new FileInfo();
            fileInfo.groupTitle = source.readString();
            fileInfo.title = source.readString();
            fileInfo.subtitle = source.readString();
            fileInfo.type = source.readInt();
            fileInfo.path = source.readString();
            fileInfo.size = source.readLong();
            fileInfo.showTitleLayout = source.readInt() == 1;
            fileInfo.isChecked = source.readInt() == 1;
            fileInfo.mediaIconResourceId = source.readInt();
            fileInfo.defaultIconResourceId = source.readInt();
            fileInfo.uri = source.readString();
            fileInfo.storage = source.readInt();
            return fileInfo;
        }

        @Override
        public FileInfo[] newArray(int size) {
            return new FileInfo[0];
        }

    };

    public static int getFolderDefaultResourceId(){
        return R.drawable.ic_filelist_folder_grey;
    }

    public static int getDefaultResourceId(String fileSubTitle){

        if(fileSubTitle != null)
            fileSubTitle = fileSubTitle.toLowerCase();

        int resId = R.drawable.ic_filelist_others_grey;

        try{
            if (isImage(fileSubTitle))
                resId = R.drawable.ic_filelist_pic_grey;
            else if (isVideo(fileSubTitle))
                resId = R.drawable.ic_filelist_video_grey;
            else if (isAudio(fileSubTitle))
                resId = R.drawable.ic_filelist_mp3_grey;
            else if(fileSubTitle.equals("xls") || fileSubTitle.equals("xlt") || fileSubTitle.equals("xlsx") || fileSubTitle.equals("xltx")){
                resId = R.drawable.ic_filelist_excel_grey;
            }
            else if(fileSubTitle.equals("ppt") || fileSubTitle.equals("pot") || fileSubTitle.equals("pps") ||
                    fileSubTitle.equals("pptx") || fileSubTitle.equals("potx") || fileSubTitle.equals("ppsx") ){
                resId = R.drawable.ic_filelist_ppt_grey;
            }
            else if(fileSubTitle.equals("doc") || fileSubTitle.equals("dot") ||
                    fileSubTitle.equals("docx") || fileSubTitle.equals("dotx")){
                resId = R.drawable.ic_filelist_word_grey;
            }
            else if(fileSubTitle.equals("zip")){
                resId = R.drawable.ic_filelist_zip_grey;
            }
            else if(fileSubTitle.equals("rar")){
                resId = R.drawable.ic_filelist_rar_grey;
            }
            else if(fileSubTitle.equals("pdf")){
                resId = R.drawable.ic_filelist_pdf_grey;
            }
            else if(fileSubTitle.equals("xml")){
                resId = R.drawable.ic_filelist_xml_grey;
            }
            else if(fileSubTitle.equals("rtf")){
                resId = R.drawable.ic_filelist_rtf_grey;
            }
            else if(fileSubTitle.equals("png")){
                resId = R.drawable.ic_filelist_pic_grey;
            }
            else{
                resId = R.drawable.ic_filelist_others_grey;
            }
        }catch (Exception e){   //似乎在沒有副檔名時會出錯
            resId = R.drawable.ic_filelist_others_grey;
        }

        return resId;
    }

    public static int getMediaIconResourceId(String fileSubTitle){
        int resId = -1;

        if(fileSubTitle != null)
            fileSubTitle = fileSubTitle.toLowerCase();

        try {
            if (isVideo(fileSubTitle))
                resId = R.drawable.ic_cameraroll_video;
            else if (isAudio(fileSubTitle))
                resId = R.drawable.ic_browser_lable_music;
        }catch(Exception e){
            resId = -1;
        }

        return  resId;
    }

    public static boolean isAudio(String fileSubTitle){
        if (fileSubTitle == null)
            return false;
        String [] audio = {"3gpp", "amr", "snd", "mid", "midi", "kar", "xmf", "mxmf", "mpga", "mpega",
                "mp2", "mp3", "m4a", "m3u", "sid", "aif", "aiff", "aifc", "gsm", "m3u", "wma",
                "wax", "ra", "rm", "ram", "pls", "sd2", "wav", "mka", "aac"};
        for (int i = 0; i < audio.length; i++){
            if (fileSubTitle.equals(audio[i])){
                return true;
            }
        }
        return false;
    }

    public static boolean isVideo(String fileSubTitle){
        if (fileSubTitle == null)
            return false;
        String [] video = {"3gpp", "3gp", "3g2", "dl", "dif", "dv", "fli", "m4v", "mpeg", "mpg", "mpe", "mp4",
                "vob", "qt", "mov", "mxu", "lsf", "lsx", "mng", "asf", "asx", "wm", "wmv", "wmx",
                "wvx", "avi", "movie", "rmvb", "rm", "rv", "flv", "hlv", "mkv", "divx", "evo", "f4v",
                "mks", "ts", "mts", "m2p", "m2t", "m2ts", "mk3d", "ogm",
                "trp", "ogv", "webm"};
        for (int i = 0; i < video.length; i++){
            if (fileSubTitle.equals(video[i])){
                return true;
            }
        }
        return false;
    }

    public static boolean isImage(String fileSubTitle){
        if (fileSubTitle == null)
            return false;
        String [] image = {"odi", "dmg", "iso", "bmp", "gif", "cur", "ico", "ief", "jpeg", "jpg", "jpe", "pcx", "png",
                "svg", "svgz", "tiff", "tif", "djvu", "djv", "wbmp", "ras", "cdr", "pat", "cdt", "cpt", "ico",
                "art", "jng", "bmp", "psd", "pnm", "pbm", "pgm", "ppm", "rgb", "xbm", "xpm", "xwd"};
        for (int i = 0; i < image.length; i++){
            if (fileSubTitle.equals(image[i])){
                return true;
            }
        }
        return false;
    }
}