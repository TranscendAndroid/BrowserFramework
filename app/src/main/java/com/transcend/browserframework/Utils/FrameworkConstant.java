package com.transcend.browserframework.Utils;

import android.os.Environment;
import android.support.v7.view.ActionMode;

/**
 * Created by mike_chen on 2017/12/21.
 */

public class FrameworkConstant {

    public static final int ITEM_LIST = 0;
    public static final int ITEM_GRID = 1;
    public static final int HEADER_ITEM = 2;
    public static final int FOOTER_ITEM = 3;

    public static final int SORT_NAME = 0;
    public static final int SORT_DATE = 1;
    public static final int SORT_SIZE = 2;
    public static final int SORT_ORDER_ASC = 0;
    public static final int SORT_ORDER_DESC = 1;

    public static String ROOT_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();

    public static int mPortraitScreenWidth;
    public static int mPortraitScreenHeight;

}
