package com.transcend.browserframework.Utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

/**
 * Created by mike_chen on 2018/1/17.
 */

public class MyPreference {

    private static final String PREFIX = "transcend-info-";
    private static final String BROWSER_VIEW_MODE_PREFIX = "browserMode-";
    public static final String BROWSER_SORT_BY_PREFIX = "browserSortBy-";
    public static final String BROWSER_SORT_ORDER_PREFIX = "browserSortOrder-";

    //顯示模式: Grid or lList
    public static int getBrowserViewType(Context context, int type, int default_value) {
        if (context == null)
            return default_value;
        String key = BROWSER_VIEW_MODE_PREFIX + type;
        return getPrefs(context).getInt(createKey(key), default_value);
    }

    public static void setBrowserViewType(Context context, int type, int viewMode) {
        if (context == null)
            return;
        String key = BROWSER_VIEW_MODE_PREFIX + type;
        getPrefs(context).edit().putInt(createKey(key), viewMode).apply();
    }
    //End of 顯示模式

    //排列依據: Name、Date of Size
    public static int getSortFactor(Context context, int default_value) {
        if (context == null)
            return default_value;
        String key = BROWSER_SORT_BY_PREFIX;
        return getPrefs(context).getInt(createKey(key), default_value);
    }

    public static void setSortFactor(Context context, int factor) {
        if (context == null)
            return;
        String key = BROWSER_SORT_BY_PREFIX;
        getPrefs(context).edit().putInt(createKey(key), factor).apply();
    }
    //End of 排列依據

    //排列順序: 升序 or 降序
    public static int getSortOrder(Context context, int default_value) {
        if (context == null)
            return default_value;
        String key = BROWSER_SORT_ORDER_PREFIX;
        return getPrefs(context).getInt(createKey(key), default_value);
    }

    public static void setSortOrder(Context context, int order) {
        if (context == null)
            return;
        String key = BROWSER_SORT_ORDER_PREFIX;
        getPrefs(context).edit().putInt(createKey(key), order).apply();
    }
    //End of 排列順序


    private static SharedPreferences getPrefs(Context context) {
        return PreferenceManager.getDefaultSharedPreferences(context);
    }

    private static String createKey(String key) {
        return PREFIX + key;
    }
}
