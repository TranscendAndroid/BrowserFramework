package com.transcend.browserframework.Utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.widget.ImageView;

public class GridImageView extends android.support.v7.widget.AppCompatImageView  {
    private OnImageChangeListiner onImageChangeListiner;

    public GridImageView(Context context) {
        super(context);
    }

    public GridImageView(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
    }

    public void setImageViewChangeListener(
            OnImageChangeListiner onImageChangeListiner) {
        this.onImageChangeListiner = onImageChangeListiner;
    }

    @Override
    public void setBackgroundResource(int resid) {
        super.setBackgroundResource(resid);
        if (onImageChangeListiner != null)
            onImageChangeListiner.imageViewChanged(this);
    }


    @Override
    public void setBackgroundDrawable(Drawable background) {
        super.setBackgroundDrawable(background);
        if (onImageChangeListiner != null && background != null)
            onImageChangeListiner.imageViewChanged(this);
    }

    @Override
    public void setImageResource(int resId) {
        super.setImageResource(resId);
        if (onImageChangeListiner != null)
            onImageChangeListiner.imageViewChanged(this);
    }

    @Override
    public void setImageBitmap(Bitmap bm) {
        super.setImageBitmap(bm);
        if (onImageChangeListiner != null && bm != null)
            onImageChangeListiner.imageViewChanged(this);
    }

    public interface OnImageChangeListiner {
        void imageViewChanged(ImageView mImageView);
    }
}
