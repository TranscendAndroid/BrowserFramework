package com.transcend.browserframework.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.transcend.browserframework.R;

import java.lang.reflect.Method;

/**
 * Created by mike_chen on 2018/1/2.
 */

public class UiHelper {

    public static View setMusicUiDimensioning(Context context, LayoutInflater mInflater){
        View contentView = null;
        contentView = mInflater.inflate(R.layout.single_view_music,null);
        musicView(contentView, context);
        return contentView;
    }

    public static View setVideoUiDimensioning(Context context, LayoutInflater mInflater){
        View contentView = null;
        contentView = mInflater.inflate(R.layout.single_view_video,null);
        videoView(contentView, context);
        return contentView;
    }

    private static void musicView(View contentView, Context context){
        RelativeLayout control_layout;

        LinearLayout info_layout;
        TextView album;
        TextView artist;

        RelativeLayout seekbar_layout;
        SeekBar mSeekbar;

        RelativeLayout time_layout;
        TextView start_time;
        TextView end_time;

        RelativeLayout btn_layout;
        ImageView play;
        ImageView prev;
        ImageView next;

        RelativeLayout btn_other;
        ImageView repeat;
        ImageView random;

        ImageView music_image;

        control_layout = (RelativeLayout)contentView.findViewById(R.id.music_control_layout);

        info_layout = (LinearLayout) contentView.findViewById(R.id.music_info_layout);
        album = (TextView) contentView.findViewById(R.id.music_album);
        artist = (TextView) contentView.findViewById(R.id.music_artist);

        seekbar_layout = (RelativeLayout) contentView.findViewById(R.id.music_seekbar_layout);
        mSeekbar = (SeekBar) contentView.findViewById(R.id.music_seekbar);

        time_layout = (RelativeLayout) contentView.findViewById(R.id.music_time_layout);
        start_time = (TextView) contentView.findViewById(R.id.music_current_time);
        end_time = (TextView) contentView.findViewById(R.id.music_duration);

        btn_layout = (RelativeLayout) contentView.findViewById(R.id.music_button_layout);
        play = (ImageView) contentView.findViewById(R.id.music_play);
        prev = (ImageView) contentView.findViewById(R.id.music_previous);
        next = (ImageView) contentView.findViewById(R.id.music_next);

        btn_other = (RelativeLayout) contentView.findViewById(R.id.music_other);
        repeat = (ImageView) contentView.findViewById(R.id.music_repeat);
        random = (ImageView) contentView.findViewById(R.id.music_shuffle);

        music_image = (ImageView) contentView.findViewById(R.id.music_image);

        UnitConverter converter = new UnitConverter(context);

        int px50 = (int) converter.convertPixelToDp(40);
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) music_image.getLayoutParams();
        lp.setMargins(px50, px50, px50, 0);
        music_image.setLayoutParams(lp);

        RelativeLayout.MarginLayoutParams relativeLp = (RelativeLayout.MarginLayoutParams) control_layout.getLayoutParams();
        relativeLp.setMargins(px50, 0, px50, 0);
        control_layout.setLayoutParams(relativeLp);

        info_layout.getLayoutParams().height = (int) converter.convertPixelToDp(120);
        album.setTextSize(converter.convertPtToSp(40));
        artist.setTextSize(converter.convertPtToSp(34));

        relativeLp = (RelativeLayout.MarginLayoutParams) seekbar_layout.getLayoutParams();
        relativeLp.setMargins(0, (int) converter.convertPixelToDp(30), 0, 0);
        seekbar_layout.setLayoutParams(relativeLp);
        seekbar_layout.getLayoutParams().height = (int) converter.convertPixelToDp(50);

        time_layout.getLayoutParams().height = (int) converter.convertPixelToDp(30);
        start_time.setTextSize(converter.convertPtToSp(24));
        end_time.setTextSize(converter.convertPtToSp(24));

        btn_layout.getLayoutParams().height = (int) converter.convertPixelToDp(148);
        play.getLayoutParams().height = (int) converter.convertPixelToDp(96);
        play.getLayoutParams().width = (int) converter.convertPixelToDp(96);
        prev.getLayoutParams().height = (int) converter.convertPixelToDp(72);
        prev.getLayoutParams().width = (int) converter.convertPixelToDp(72);
        next.getLayoutParams().height = (int) converter.convertPixelToDp(72);
        next.getLayoutParams().width = (int) converter.convertPixelToDp(72);

        btn_other.getLayoutParams().height = (int) converter.convertPixelToDp(98);
        repeat.getLayoutParams().width = (int) converter.convertPixelToDp(95);
        repeat.getLayoutParams().height = (int) converter.convertPixelToDp(95);
        repeat.setPadding((int) converter.convertPixelToDp(5),(int) converter.convertPixelToDp(5),(int) converter.convertPixelToDp(25),(int) converter.convertPixelToDp(5));
        random.getLayoutParams().width = (int) converter.convertPixelToDp(95);
        random.getLayoutParams().height = (int) converter.convertPixelToDp(95);
        random.setPadding((int) converter.convertPixelToDp(25),(int) converter.convertPixelToDp(5),(int) converter.convertPixelToDp(5),(int) converter.convertPixelToDp(5));
        relativeLp = (RelativeLayout.MarginLayoutParams) btn_other.getLayoutParams();
        relativeLp.setMargins((int) converter.convertPixelToDp(20), 0, (int) converter.convertPixelToDp(20), 0);
        btn_other.setLayoutParams(relativeLp);

        int px116 = (int) converter.convertPixelToDp(116);
        lp = (ViewGroup.MarginLayoutParams) prev.getLayoutParams();
        lp.setMargins(0,0,px116,0);
        prev.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) next.getLayoutParams();
        lp.setMargins(px116,0,0,0);
        next.setLayoutParams(lp);
    }

    private static void videoView(View contentView, Context context){

        TextView mToolbarTitle;

        RelativeLayout control_layout;
        ImageView play, prev, next, prev15, next15, download, delete;

        RelativeLayout seekbar_layout;
        SeekBar mSeekbar;
        TextView current_time, duration;

        ImageView image;

        control_layout = (RelativeLayout) contentView.findViewById(R.id.video_control_layout);
        play = (ImageView) contentView.findViewById(R.id.video_play);
        prev = (ImageView) contentView.findViewById(R.id.video_previous);
        next = (ImageView) contentView.findViewById(R.id.video_next);
        prev15 = (ImageView) contentView.findViewById(R.id.video_previous15);
        next15 = (ImageView) contentView.findViewById(R.id.video_next15);
        download = (ImageView) contentView.findViewById(R.id.video_download);
        delete = (ImageView) contentView.findViewById(R.id.video_delete);

        seekbar_layout = (RelativeLayout) contentView.findViewById(R.id.video_seekbar_layout);
        mSeekbar = (SeekBar) contentView.findViewById(R.id.video_seekbar);

        current_time = (TextView) contentView.findViewById(R.id.video_current_time);
        duration = (TextView) contentView.findViewById(R.id.video_duration);

        image = (ImageView) contentView.findViewById(R.id.video_image);

        UnitConverter converter = new UnitConverter(context);

        control_layout.getLayoutParams().height = (int) converter.convertPixelToDp(106);

        int px8 = (int) converter.convertPixelToDp(8);
        seekbar_layout.setPadding(px8, 0, px8, 0);
        seekbar_layout.getLayoutParams().height = (int) converter.convertPixelToDp(60);
        current_time.setTextSize(converter.convertPtToSp(24));
        duration.setTextSize(converter.convertPtToSp(24));

        int LengthAndWidth = (int) converter.convertPixelToDp(70);
        play.getLayoutParams().height = LengthAndWidth;
        play.getLayoutParams().width = LengthAndWidth;
        prev.getLayoutParams().height = LengthAndWidth;
        prev.getLayoutParams().width = LengthAndWidth;
        next.getLayoutParams().height = LengthAndWidth;
        next.getLayoutParams().width = LengthAndWidth;
        prev15.getLayoutParams().height = LengthAndWidth;
        prev15.getLayoutParams().width = LengthAndWidth;
        next15.getLayoutParams().height = LengthAndWidth;
        next15.getLayoutParams().width = LengthAndWidth;
        download.getLayoutParams().width = LengthAndWidth;
        download.getLayoutParams().height = LengthAndWidth;
        delete.getLayoutParams().width = LengthAndWidth;
        delete.getLayoutParams().height = LengthAndWidth;

        int px32 = (int) converter.convertPixelToDp(32);
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) download.getLayoutParams();
        lp.setMargins(px32, 0, 0, 0);
        download.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) delete.getLayoutParams();
        lp.setMargins(0, 0, px32, 0);
        delete.setLayoutParams(lp);
    }

    //此處特別注意，如果長寬都為0會無法處理
    public static int calculateGridItemWidth(Context context){
        int columnCount, screenWidth;

        if(isPad(context)){
            if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                screenWidth = (FrameworkConstant.mPortraitScreenWidth > FrameworkConstant.mPortraitScreenHeight) ? FrameworkConstant.mPortraitScreenWidth : FrameworkConstant.mPortraitScreenHeight;
                columnCount = 8;
            }
            else {
                screenWidth = (FrameworkConstant.mPortraitScreenWidth < FrameworkConstant.mPortraitScreenHeight) ? FrameworkConstant.mPortraitScreenWidth : FrameworkConstant.mPortraitScreenHeight;
                columnCount = 6;
            }
        }
        else{
            if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
                screenWidth = (FrameworkConstant.mPortraitScreenWidth > FrameworkConstant.mPortraitScreenHeight) ? FrameworkConstant.mPortraitScreenWidth : FrameworkConstant.mPortraitScreenHeight;
                columnCount = 6;
            }
            else {
                screenWidth = (FrameworkConstant.mPortraitScreenWidth < FrameworkConstant.mPortraitScreenHeight) ? FrameworkConstant.mPortraitScreenWidth : FrameworkConstant.mPortraitScreenHeight;
                columnCount = 3;
            }
        }
        return screenWidth / columnCount;
    }

    public static int getStatusBarHeight(Context mContext){
        // status bar height
        int statusBarHeight = 0;
        int resourceId = mContext.getResources().getIdentifier("status_bar_height", "dimen", "android");
        if (resourceId > 0) {
            statusBarHeight = mContext.getResources().getDimensionPixelSize(resourceId);
        }
        return statusBarHeight;
    }

    public static int getStatusBarCount(Context context){
        return (int) (24 * context.getResources().getDisplayMetrics().density);
    }

    public static int getNotchHeight(Context context){
        int systemHeight = getStatusBarHeight(context);
        int countHeight = getStatusBarCount(context);
        if ((systemHeight - countHeight) > systemHeight/2 || (systemHeight - countHeight) > countHeight/2)
            return (systemHeight - countHeight);
        return 0;
    }

    public static int getNavigationBarHeight(Context mContext){
        // navigation bar height
        int navigationBarHeight = 0;
        boolean hasNavigationBar = checkDeviceHasNavigationBar(mContext);
        if (!hasNavigationBar)
            return 0;
        int resourceId = mContext.getResources().getIdentifier("navigation_bar_height", "dimen", "android");
        if (resourceId > 0) {
            navigationBarHeight = mContext.getResources().getDimensionPixelSize(resourceId);
        }

        return navigationBarHeight;
    }

    //判斷是否有navigation bar
    private static boolean checkDeviceHasNavigationBar(Context context) {
        boolean hasNavigationBar = false;
        Resources rs = context.getResources();
        int id = rs.getIdentifier("config_showNavigationBar", "bool", "android");
        if (id > 0) {
            hasNavigationBar = rs.getBoolean(id);
        }
        try {
            Class systemPropertiesClass = Class.forName("android.os.SystemProperties");
            Method m = systemPropertiesClass.getMethod("get", String.class);
            String navBarOverride = (String) m.invoke(systemPropertiesClass, "qemu.hw.mainkeys");
            if ("1".equals(navBarOverride)) {
                hasNavigationBar = false;
            } else if ("0".equals(navBarOverride)) {
                hasNavigationBar = true;
            }
        } catch (Exception e) {
            //Log.w(TAG, e);
        }

        return hasNavigationBar;
    }

    public static void setSystemBarTranslucent(Activity mActivity){
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            Window window = mActivity.getWindow();
            // Translucent status bar
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
            // Translucent navigation bar
            window.setFlags(
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION,
                    WindowManager.LayoutParams.FLAG_TRANSLUCENT_NAVIGATION);
        }
    }

    public static void hideBothSystemBar(Activity mActivity){
        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_FULLSCREEN | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public static void showBothSystemBar(Activity mActivity){
        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public static void hideBothSystemBarWithoutTranslucent(Activity mActivity){
        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                | View.SYSTEM_UI_FLAG_IMMERSIVE;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public static void showBothSystemBarWithoutTranslucent(Activity mActivity){
        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        decorView.setSystemUiVisibility(uiOptions);
    }

    public static void hideNavigationBarOnly(Activity mActivity){
        //隱藏Navigation bar (Back, Home, Apps)
        View decorView = mActivity.getWindow().getDecorView();
        int uiOptions = View.SYSTEM_UI_FLAG_HIDE_NAVIGATION | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        decorView.setSystemUiVisibility(uiOptions);
    }

    //回傳true就是平板、回傳false就是手機
    public static boolean isPad(Context context) {
        return (context.getResources().getConfiguration().screenLayout
                & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public static void updateScreenSize(Activity mActivity){
        //目前好像只能在activity取得螢幕寬度
        DisplayMetrics displaymetrics = new DisplayMetrics();
        mActivity.getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
        int mScreenW = displaymetrics.widthPixels;
        int mScreenH = displaymetrics.heightPixels;
        FrameworkConstant.mPortraitScreenWidth = mScreenW;
        FrameworkConstant.mPortraitScreenHeight = mScreenH;
    }
}