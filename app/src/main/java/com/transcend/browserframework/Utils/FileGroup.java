package com.transcend.browserframework.Utils;

import java.util.ArrayList;

/**
 * Created by mike_chen on 2018/1/17.
 */

public class FileGroup {

    public String title;
    private ArrayList<FileInfo> mList;

    public FileGroup(String title){
        mList = new ArrayList<FileInfo>();
        this.title = title;
    }

    public void addFile(FileInfo fileinfo){
        mList.add(fileinfo);
    }

    public ArrayList<FileInfo> getList(){
        return mList;
    }

    public void setList(ArrayList<FileInfo> list){
        mList.clear();
        mList = list;
    }

    public int getListSize(){
        return mList.size();
    }

}
