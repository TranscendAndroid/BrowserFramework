package com.transcend.browserframework;

import com.transcend.browserframework.Browser.ToolbarController;

public class FrameworkBuilder {

    //必須設置以下三項: layout id, 標題, Toolbar模式
    private int mLayoutID;
    private String mTitle;
    private ToolbarController.ToolbarMode mToolbarMode = ToolbarController.ToolbarMode.NORMAL;

    //設置layout id
    public FrameworkBuilder setLayoutID(int layoutID) {
        mLayoutID = layoutID;
        return this;
    }

    //取得layout id
    public int getLayoutID() {
        return mLayoutID;
    }

    //設置標題
    public FrameworkBuilder setTitle(String title) {
        mTitle = title;
        return this;
    }

    //取得標題
    public String getTitle() {
        if (mTitle == null)
            return "";
        return mTitle;
    }

    //設置Toolbar模式
    public FrameworkBuilder setToolbarMode(ToolbarController.ToolbarMode mode) {
        mToolbarMode = mode;
        return this;
    }

    //取得Toolbar模式
    public ToolbarController.ToolbarMode getToolbarMode() {
        return mToolbarMode;
    }
}
