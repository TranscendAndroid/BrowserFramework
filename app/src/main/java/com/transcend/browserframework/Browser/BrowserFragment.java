package com.transcend.browserframework.Browser;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.CustomViewPager;
import com.transcend.browserframework.Utils.FileInfo;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.PagerSwipeRefreshLayout;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by mike_chen on 2017/12/20.
 */

public class BrowserFragment extends Fragment implements TabInfo.OnRecyclerViewScrollListener{
    private String TAG = BrowserFragment.class.getSimpleName();

    private PagerSwipeRefreshLayout mSwipeRefreshLayout;    //下滑刷新，並避免與ViewPage的滑動衝突
    private boolean isEnableSwipeRefresh = true;
    private CustomViewPager mViewPager;   //tab layout pager
    private TabLayout mTabLayout;
    private RelativeLayout tab_layout;
    private final ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
    private TabInfo mCurTab = null;
    private int mCurrentTabPosition = 0;
    private String mPath;
    private boolean isTabClickable = true;
    private boolean isChangedPath = false;

    private LayoutInflater mInflater;

    public static final String START_POSITION = "start_position";

    //BrowserFragmentStartLoadingFiles: state
    static public int StartLoadingFiles_Init = 0;
    static public int StartLoadingFiles_EnterFolder = 1;
    static public int StartLoadingFiles_PagerChanged = 2;
    static public int StartLoadingFiles_SwipeRefresh = 3;

    private BrowserFragmentListener mBrowserFragmentListener;
    public interface BrowserFragmentListener{
        void BrowserFragmentStartLoadingFiles(int tab_position, String path, int state);
        void BrowserFragmentViewPagerChanged(int pre_tab_position, int post_tab_position);
        void BrowserFragmentRecyclerViewScrolledBottom(int current_position);
        void BrowserFragmentRecyclerViewScrollStateChanged(RecyclerView recyclerView, int newState);
    }
    public void setBrowserFragmentListener(BrowserFragmentListener listener){
        mBrowserFragmentListener = listener;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPath = FrameworkConstant.ROOT_PATH;
    }

    //更改根目錄
    public void setRootPath(String path){
        mPath = FrameworkConstant.ROOT_PATH = path;
    }

    //更改ListView or GridView
    public void setViewMode(int viewType) {
        mTabs.get(mCurrentTabPosition).updateLayoutType(viewType);
    }

    @Override   //onSaveInstanceState()會在activity被銷毀之前執行
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("key_mCurrentTabPosition", mCurrentTabPosition);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        mInflater = inflater;
        CoordinatorLayout root = (CoordinatorLayout) inflater.inflate(R.layout.fragment_browser, container, false);

        mSwipeRefreshLayout = root.findViewById(R.id.swiperefresh);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.c_06);

        if (!isEnableSwipeRefresh)
            mSwipeRefreshLayout.setEnabled(false);

        mViewPager = root.findViewById(R.id.viewPager);
        mTabLayout = root.findViewById(R.id.tabLayout);
        tab_layout = root.findViewById(R.id.tab_content_layout);

        if (savedInstanceState != null)
            mCurrentTabPosition = savedInstanceState.getInt("key_mCurrentTabPosition");

        MyPagerAdapter adapter = new MyPagerAdapter(mTabs, mInflater);
        mViewPager.setAdapter(adapter);
        mViewPager.addOnPageChangeListener(adapter);
        //檢查bundle是否為null
        if (getArguments() != null) {
            mCurrentTabPosition = getArguments().getInt(START_POSITION);
        }
        mViewPager.setCurrentItem(mCurrentTabPosition);

        //設置tab type圖示；如果Tab的數量只有一個，則隱藏TabLayout
        mTabLayout.setupWithViewPager(mViewPager);
        setTabLayoutEnable(isTabClickable);
        for (int i = 0; i < mTabLayout.getTabCount(); i++) {
            mTabLayout.getTabAt(i).setIcon(mTabs.get(i).getIconId());
        }
        if (mTabLayout.getTabCount()>1)
            tab_layout.setVisibility(View.VISIBLE);
        else
            tab_layout.setVisibility(View.GONE);

        mCurTab = mTabs.get(mCurrentTabPosition);

        //讀取資料
        mBrowserFragmentListener.BrowserFragmentStartLoadingFiles(mCurrentTabPosition, mPath, StartLoadingFiles_Init);

        return root;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mSwipeRefreshLayout.setRefreshing(false);
                mBrowserFragmentListener.BrowserFragmentStartLoadingFiles(mCurrentTabPosition, mPath, StartLoadingFiles_SwipeRefresh);
            }
        });
    }

    //更新目前Tab的資料
    public void updateData(ArrayList<FileInfo> data){
        if(data != null && mCurTab!=null) {
            mCurTab.updateTabData(data);

            //更新歷史路徑
            if (isChangedPath) {
                isChangedPath = false;
            }
        }
    }

    public void appendData(ArrayList<FileInfo> data){
        mCurTab.appendData(data);
    }

    public void doLoad(String path){
        mPath = path;
        isChangedPath = true;
        mBrowserFragmentListener.BrowserFragmentStartLoadingFiles(mCurrentTabPosition, path, StartLoadingFiles_EnterFolder);
    }

    //History page 判斷是否在根目錄
    public boolean isOnRootPage(){
        if (mPath == null)
            return true;
        if(mPath.equals(FrameworkConstant.ROOT_PATH))
            return true;
        else {
            return false;
        }
    }

    class MyPagerAdapter extends PagerAdapter implements ViewPager.OnPageChangeListener {
        private ArrayList<TabInfo> mTabs = new ArrayList<TabInfo>();
        private LayoutInflater mInflater;

        MyPagerAdapter(ArrayList<TabInfo> tabs, LayoutInflater inflater) {
            mTabs = tabs;
            mInflater = inflater;
        }

        @Override
        public int getCount() {
            return mTabs.size();
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            TabInfo tab = mTabs.get(position);
            View root = tab.getRootView();
            container.addView(root);

            return root;
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View)object);
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view == object;
        }

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
        }

        @Override
        public void onPageSelected(int position) {
            boolean needLoad = (mCurrentTabPosition != position);
            mCurTab = mTabs.get(position);
            int prePosition = mCurrentTabPosition;
            mCurrentTabPosition = position;

            if(needLoad) {
                mBrowserFragmentListener.BrowserFragmentStartLoadingFiles(mCurrentTabPosition, mPath, StartLoadingFiles_PagerChanged);
            }

            mBrowserFragmentListener.BrowserFragmentViewPagerChanged(prePosition, position);
        }

        @Override
        public void onPageScrollStateChanged(int state) {
            if (mSwipeRefreshLayout != null) {
                if (isEnableSwipeRefresh)
                    mSwipeRefreshLayout.setEnabled(state == ViewPager.SCROLL_STATE_IDLE);
            }
        }
    }

    //移至指定Tab
    public void moveToTab(int position){
        mViewPager.setCurrentItem(position);
    }

    public void addTab(TabInfo tab){
        tab.setOnRecyclerScrollListener(this);
        mTabs.add(tab);
    }

    //控制讀取符號可見度
    public void setLoadingProgressVisibility(int visibility){
        if (visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            mCurTab.setLoadingProgressVisibility(visibility);
    }

    @Override
    public void RecyclerViewScrollBottom() {
        mBrowserFragmentListener.BrowserFragmentRecyclerViewScrolledBottom(mCurrentTabPosition);
    }

    @Override
    public void RecyclerViewScrollStateChanged(RecyclerView recyclerView, int newState) {
        mBrowserFragmentListener.BrowserFragmentRecyclerViewScrollStateChanged(recyclerView, newState);
    }

    //回傳是否正在更新中
    public boolean isRefreshing(){
        return mSwipeRefreshLayout.isRefreshing();
    }

    //設置刷新頁面是否啟用
    public void setSwipeRefreshEnable(boolean enable){
        isEnableSwipeRefresh = enable;
        if (mSwipeRefreshLayout != null)
            mSwipeRefreshLayout.setEnabled(enable);
    }

    public TabInfo getCurrentTab(){
        return mCurTab;
    }

    public TabInfo getTabAtPosition(int position){
        return mTabs.get(position);
    }

    public int getCurrentTabPosition(){
        return mCurrentTabPosition;
    }

    public void deselectAllItems(){
        if (mCurrentTabPosition < mTabs.size())
            if (mTabs.get(mCurrentTabPosition) != null)
                mTabs.get(mCurrentTabPosition).deselectAll();
    }

    public void selectAllItems(){
        if (mCurrentTabPosition < mTabs.size())
            if (mTabs.get(mCurrentTabPosition) != null)
                mTabs.get(mCurrentTabPosition).selectAll();
    }

    public boolean isSelectAll(){
        if (mCurrentTabPosition < mTabs.size())
            if (mTabs.get(mCurrentTabPosition) != null)
                return mTabs.get(mCurrentTabPosition).isSelectAll();
        return false;
    }

    public ArrayList<FileInfo> getSelectedFiles(){
        if (mCurrentTabPosition < mTabs.size()) {
            if (mTabs.get(mCurrentTabPosition) != null)
                return mTabs.get(mCurrentTabPosition).getSelectedFiles();
        }
        return new ArrayList<FileInfo>();
    }

    public int getCurrentTabType(){
        if (mCurTab == null)
            return -1;
        return mCurTab.getType();
    }

    public ArrayList<FileInfo> getCurrentTabFileList(){
        if (mCurTab == null)
            return new ArrayList<FileInfo>();
        return mCurTab.getFileList();
    }

    public ArrayList<FileInfo> getFileListWithType(int type){
        if (mCurTab == null)
            return new ArrayList<FileInfo>();
        return mCurTab.getFileListWithType(type);
    }

    public void refreshCurrentAdapter(){
        if (mCurTab != null)
            mCurTab.getAdapter().notifyDataSetChanged();
    }

    public String getPath(){
        return mPath;
    }

    //進入目前tab的選取模式
    public void setSelectMode(boolean enable){
        if (mCurTab != null)
            mCurTab.getAdapter().setSelectMode(enable);
    }

    //進入position的選取模式
    public void setSelectMode(int position, boolean enable){
        if (position < mTabs.size())
            mTabs.get(position).getAdapter().setSelectMode(enable);
    }

    public void setTabLayoutEnable(final boolean enable){
        isTabClickable = enable;
        if (mTabLayout != null) {
            LinearLayout tabStrip = ((LinearLayout) mTabLayout.getChildAt(0));
            for(int i = 0; i < tabStrip.getChildCount(); i++) {
                tabStrip.getChildAt(i).setOnTouchListener(new View.OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        return !enable;
                    }
                });
            }

            mViewPager.setPagingEnabled(enable);
        }
    }

    //修改指定tab裡的無檔案圖示
    public void setEmptyImageResource(int position, int resId){
        if (position < mTabs.size()) {
            if (mTabs.get(position) == null)
                return;

            mTabs.get(position).setEmptyImage(resId);
        }
    }
}