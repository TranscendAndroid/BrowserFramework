package com.transcend.browserframework.Browser;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UnitConverter;
import com.transcend.browserframework.Utils.MyPreference;

import java.lang.ref.WeakReference;

/**
 * Created by mike_chen on 2018/2/5.
 */

public class SortDialog {

    private WeakReference<Context> mContext;

    public SortDialog(Context context){
        mContext = new WeakReference<>(context);
    }

    private OnSortListener mSortListener;
    public interface OnSortListener{
        void sort();
    }
    public void setSortListener(OnSortListener listener){
        mSortListener = listener;
    }

    public void showSortDialog(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        UnitConverter format = new UnitConverter(context);

        final Dialog dialog = new Dialog(context);//指定自定義樣式
        dialog.setContentView(R.layout.sort_layout);//指定自定義layout

        final RadioButton name = (RadioButton) dialog.findViewById(R.id.sort_name);
        final RadioButton date = (RadioButton) dialog.findViewById(R.id.sort_date);
        final RadioButton size = (RadioButton) dialog.findViewById(R.id.sort_size);
        final RadioButton asc = (RadioButton) dialog.findViewById(R.id.sort_asc);
        final RadioButton desc = (RadioButton) dialog.findViewById(R.id.sort_desc);

        RadioGroup factor = (RadioGroup) dialog.findViewById(R.id.sort_factor);
        RadioGroup order = (RadioGroup) dialog.findViewById(R.id.sort_order);
        LinearLayout btnLayout = (LinearLayout) dialog.findViewById(R.id.sort_btn_layout);

        LinearLayout layoutManager = (LinearLayout) dialog.findViewById(R.id.sort_layout_manager);

        TextView ok = (TextView) dialog.findViewById(R.id.sort_ok);
        TextView cancel = (TextView) dialog.findViewById(R.id.sort_cancel);

        //UI setup
        int px24 = (int) format.convertPixelToDp(24);
        int px48 = (int) format.convertPixelToDp(48);

        LinearLayout.MarginLayoutParams lp = (LinearLayout.MarginLayoutParams) layoutManager.getLayoutParams();
        lp.setMargins(px48,px24,px48,20);
        lp.width = (int) format.convertPixelToDp(550);
        layoutManager.setLayoutParams(lp);

        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) factor.getLayoutParams();
        layoutParams.setMargins(0,0,0,px24);
        factor.setLayoutParams(layoutParams);

        name.setPadding(px24,0,0,0);
        date.setPadding(px24,0,0,0);
        size.setPadding(px24,0,0,0);
        asc.setPadding(px24,0,0,0);
        desc.setPadding(px24,0,0,0);

        name.setTextSize((int)format.convertPtToSp(40));
        name.setTextColor(context.getResources().getColor(R.color.c_02));
        date.setTextSize((int)format.convertPtToSp(40));
        date.setTextColor(context.getResources().getColor(R.color.c_02));
        size.setTextSize((int)format.convertPtToSp(40));
        size.setTextColor(context.getResources().getColor(R.color.c_02));
        asc.setTextSize((int)format.convertPtToSp(32));
        asc.setTextColor(context.getResources().getColor(R.color.c_02));
        desc.setTextSize((int)format.convertPtToSp(32));
        desc.setTextColor(context.getResources().getColor(R.color.c_02));

        ok.setTextSize((int)format.convertPtToSp(34));
        ok.setTextColor(context.getResources().getColor(R.color.c_06));
        cancel.setTextSize((int)format.convertPtToSp(34));
        cancel.setTextColor(context.getResources().getColor(R.color.c_06));

        factor.getLayoutParams().height = (int)format.convertPixelToDp(264);
        order.getLayoutParams().height = (int)format.convertPixelToDp(104);
        btnLayout.getLayoutParams().height = (int)format.convertPixelToDp(104);
        //End of UI setup

        //設置預設排序或紀錄過的排序
        int sortFactor = MyPreference.getSortFactor(context, FrameworkConstant.SORT_NAME);
        int sortOrder = MyPreference.getSortOrder(context, FrameworkConstant.SORT_ORDER_ASC);
        factor.clearCheck();
        order.clearCheck();
        switch (sortFactor){
            case FrameworkConstant.SORT_NAME:
                name.setChecked(true);
                break;
            case FrameworkConstant.SORT_DATE:
                date.setChecked(true);
                break;
            default:
                size.setChecked(true);
                break;
        }
        if(sortOrder== FrameworkConstant.SORT_ORDER_ASC)
            asc.setChecked(true);
        else
            desc.setChecked(true);
        //End

        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Context context = mContext.get();
                if(context != null){//判斷有無被系統GC
                    context = mContext.get();
                    //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
                }
                else
                    return;

                if(name.isChecked())
                    MyPreference.setSortFactor(context, FrameworkConstant.SORT_NAME);
                else if(date.isChecked())
                    MyPreference.setSortFactor(context, FrameworkConstant.SORT_DATE);
                else
                    MyPreference.setSortFactor(context, FrameworkConstant.SORT_SIZE);

                if(asc.isChecked())
                    MyPreference.setSortOrder(context, FrameworkConstant.SORT_ORDER_ASC);
                else
                    MyPreference.setSortOrder(context, FrameworkConstant.SORT_ORDER_DESC);

                if (mSortListener == null)
                    Toast.makeText(context, "Not set listener yet!!", Toast.LENGTH_SHORT).show();
                else
                    mSortListener.sort();

                dialog.dismiss();
            }
        });

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        //顯示dialog
        dialog.show();
    }
}
