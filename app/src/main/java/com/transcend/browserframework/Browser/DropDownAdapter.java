package com.transcend.browserframework.Browser;

import android.content.Context;
import android.graphics.Color;
import android.text.TextUtils;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UnitConverter;

import java.lang.ref.WeakReference;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Created by mike_chen on 2018/1/16.
 */

public class DropDownAdapter extends BaseAdapter {

    String TAG = DropDownAdapter.class.getSimpleName();

    private Spinner mDropdown;
    private List<String> mList;
    private WeakReference<Context> mContext;

    private ImageView historyPage_arrow;

    //初始位置名稱
    private String PREFIX_MAINPAGE = "Fragment";

    private OnDropdownItemSelectedListener dropdownItemListener;
    public interface OnDropdownItemSelectedListener {
        void onDropdownItemSelected(int position);
    }

    public DropDownAdapter(Context context, ImageView arrow){
        mList = new ArrayList<String>();
        mContext = new WeakReference<>(context);
        historyPage_arrow = arrow;
    }

    //設置初始目錄的標題
    public void setMainPage(String title){
        int count = 0;
        for (String a : mList){
            if (a.equals(PREFIX_MAINPAGE)) {
                mList.set(count, title);
                break;
            }
            count++;
        }
        PREFIX_MAINPAGE = title;
        notifyDataSetChanged();
    }

    public void setOnDropdownItemSelectedListener(OnDropdownItemSelectedListener listener) {
        dropdownItemListener = listener;
    }

    //以路徑更新列表
    public void updateDropDownList(String path) {
        path = path.replaceFirst(FrameworkConstant.ROOT_PATH, PREFIX_MAINPAGE);

        List<String> list = new ArrayList<String>();
        String[] items = path.split("/");
        list = Arrays.asList(items);
        Collections.reverse(list);
        mList = list;
    }

    //以列表更新列表
    public void updateDropDownList(List<String> list){
        mList = list;
    }

    public void resetList(){
        mList = new ArrayList<>();
    }

    public String getPath(int position) {
        List<String> list = mList.subList(position, mList.size());
        Collections.reverse(list);
        StringBuilder builder = new StringBuilder();
        for (String item : list) {
            builder.append(item);
            builder.append("/");
        }
        String path = builder.toString();

        path = path.replaceFirst(PREFIX_MAINPAGE, FrameworkConstant.ROOT_PATH);

        if (path.endsWith("/")) {
            path = path.substring(0, path.length() - 1);
        }
        return path;
    }

    @Override
    public int getCount() {
        return mList.size();
    }

    @Override
    public Object getItem(int position) {
        return mList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return convertView;

        if (parent instanceof Spinner)
            mDropdown = (Spinner) parent;
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            View view = inflater.inflate(R.layout.history_dropdown_page, parent, false);
            convertView = ViewHolder.get(view, R.id.dropdown_text);
        }

        ((TextView) convertView).setText(mList.get(0));
        ((TextView) convertView).setTextColor(Color.WHITE);

        UnitConverter converter = new UnitConverter(context);
        ((TextView) convertView).setTextSize(converter.convertPtToSp(38));
        ((TextView) convertView).setSingleLine(true);
        ((TextView) convertView).setEllipsize(TextUtils.TruncateAt.valueOf("END")); //內容過長，End部位以...省略

        if (mList.size() == 1) {
            mDropdown.setEnabled(false);
            historyPage_arrow.setVisibility(View.INVISIBLE);
        }
        else {
            mDropdown.setEnabled(true);
            historyPage_arrow.setVisibility(View.VISIBLE);
        }

        return convertView;
    }

    @Override
    public View getDropDownView(int position, View convertView, final ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            convertView = inflater.inflate(R.layout.history_dropdown_page, parent, false);
        }
        TextView tv = ViewHolder.get(convertView, R.id.dropdown_text);
        tv.setText(mList.get(position));

        ImageView iv = ViewHolder.get(convertView, R.id.dropdown_icon);

        if(position == 0)   //目前位置
            iv.setImageResource(R.drawable.ic_tab_location_solid_grey);
        else if(position == mList.size() - 1)   //根目錄位置
            iv.setImageResource(R.drawable.ic_tab_home_solid_grey);
        else    //其餘皆以資料夾顯示
            iv.setImageResource(R.drawable.ic_tab_folder_solid_grey);

        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return convertView;

        //UI調整
        UnitConverter converter = new UnitConverter(context);
        LinearLayout layout_manager = ViewHolder.get(convertView, R.id.dropdown_layout_manager);
        layout_manager.getLayoutParams().height = (int) converter.convertPixelToDp(88);

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) iv.getLayoutParams();
        lp.setMargins((int)converter.convertPixelToDp(36), 0, (int)converter.convertPixelToDp(20), 0);
        iv.setLayoutParams(lp);

        tv.setPadding(0,0, (int) converter.convertPixelToDp(72),0);

        tv.setTextSize(converter.convertPtToSp(36));
        tv.setSingleLine(true);
        tv.setEllipsize(TextUtils.TruncateAt.valueOf("END"));

        layout_manager.setOnTouchListener(new OnDropdownItemTouchListener(position));
        //End of UI調整

        return convertView;
    }

    public static class ViewHolder {
        @SuppressWarnings("unchecked")
        public static <T extends View> T get(View view, int id) {
            SparseArray<View> holder = (SparseArray<View>) view.getTag();
            if (holder == null) {
                holder = new SparseArray<View>();
                view.setTag(holder);
            }
            View child = holder.get(id);
            if (child == null) {
                child = view.findViewById(id);
                holder.put(id, child);
            }
            return (T) child;
        }
    }

    public class OnDropdownItemTouchListener implements View.OnTouchListener {

        private int mPosition;

        public OnDropdownItemTouchListener(int position) {
            mPosition = position;
        }

        @Override
        public boolean onTouch(View v, MotionEvent event) {
            if (event.getAction() == MotionEvent.ACTION_UP) {
                if (dropdownItemListener != null) {
                    dropdownItemListener.onDropdownItemSelected(mPosition);
                }
                dismissDropdownList();
            }
            return true;
        }

        /**
         * In order to make dropdown list scrollable,
         * onDropdownItemSelected callback should be called in ACTION_UP instead of ACTION_DOWN.
         * That causes one problem that dropdown list would not dismiss automatically.
         * One solution is to detach spinner from window by reflection method,
         * and dropdown list will disappear.
         */
        private void dismissDropdownList() {
            try {
                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                method.setAccessible(true);
                method.invoke(mDropdown);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getTopText(){
        if (mList.size() > 0)
            return mList.get(0);
        else
            return "";
    }
}
