package com.transcend.browserframework.Browser;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Point;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.FileInfo;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.GridImageView;
import com.transcend.browserframework.Utils.MyPreference;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by mike_chen on 2018/1/17.
 */

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {
    String TAG = RecyclerViewAdapter.class.getSimpleName();

    private ArrayList<FileInfo> mList = new ArrayList<FileInfo>();
    private HashMap<Integer, String> mSection = null;

    private WeakReference<Context> mContext;

    private int selectedItemCount = 0;

    private int mType = -1;

    private boolean isSelectMode = false;

    //監聽item點擊事件
    private OnRecyclerItemCallbackListener mRecyclerItemCallback;
    public interface OnRecyclerItemCallbackListener {   //customize listener
        void onRecyclerItemClick(FileInfo file, int position);
        void onRecyclerItemLongClick(FileInfo file, int position);
        void onRecyclerInfoClick(FileInfo file, int position);
        void onRecyclerThumbnailClick(FileInfo file, int position);
        void onViewRecycled(@NonNull ViewHolder holder);
    }
    public void setOnRecyclerItemCallbackListener(OnRecyclerItemCallbackListener listener){
        mRecyclerItemCallback = listener;
    }

    //監聽讀取Thumbnail事件
    private OnThumbnailCallbackListener mThumbnailCallback;
    public interface OnThumbnailCallbackListener{
        //輸入參數依序為: context、檔案路徑、檔案序列、檔案圖示、檔案類型、圖示大小
        void loadThumbnail(Context context, String filePath, int position, ImageView iconThumb, int thumbType, Point thumbSize);
        Bitmap loadBitmapCache(String key, ImageView iconView);
    }
    public void setOnThumbnailCallbackListener(OnThumbnailCallbackListener listener){
        mThumbnailCallback = listener;
    }

    public RecyclerViewAdapter(Context context, int type) {
        mContext = new WeakReference<Context>(context);
        mType = type;
        mRecyclerItemCallback = (OnRecyclerItemCallbackListener) context;
        mThumbnailCallback = (OnThumbnailCallbackListener) context;
    }

    @Override
    public RecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater mLayoutInflater = LayoutInflater.from(parent.getContext());

        switch (viewType){
            case FrameworkConstant.ITEM_LIST:
                return new ViewHolder(mLayoutInflater.inflate(R.layout.recyclerview_listitem, parent, false), viewType);
            case FrameworkConstant.ITEM_GRID:
                return new ViewHolder(mLayoutInflater.inflate(R.layout.recyclerview_griditem, parent, false), viewType);
            case FrameworkConstant.HEADER_ITEM:
                return new ViewHolder(mLayoutInflater.inflate(R.layout.grid_group_section, parent, false), viewType);
            case FrameworkConstant.FOOTER_ITEM:
                return new ViewHolder(mLayoutInflater.inflate(R.layout.footitem_file_manage, parent, false), viewType);
            default:
                break;
        }

        return null;
    }

    protected void setViewType(int type){
        notifyDataSetChanged();
    }

    private int positionToSectionPosition(int position){
        int target_position = position;
        for (Integer pos : mSection.keySet()){
            if (pos < position)
                target_position--;
        }
        return target_position;
    }

    @Override
    public void onBindViewHolder(RecyclerViewAdapter.ViewHolder holder, int position) {
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        //設定群組標題
        if (holder.viewType == FrameworkConstant.HEADER_ITEM){
            //UI調整
            UnitConverter format = new UnitConverter(context);

            //UI design
            holder.title.setText(mSection.get(position));

            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.title.getLayoutParams();
            lp.setMargins((int) format.convertPixelToDp(32f),0,0,0);
            holder.title.setLayoutParams(lp);

            holder.title.getLayoutParams().height = (int)format.convertPixelToDp(88);
            holder.title.setTextSize((int)format.convertPtToSp(34));
            holder.title.setTextColor(context.getResources().getColor(R.color.c_03));

            return;
        }

        //footer不用設置
        if (holder.viewType == FrameworkConstant.FOOTER_ITEM)
            return;

        //調整分組後的position，沒有分組就不用調整
        if (mSection != null)
            position = positionToSectionPosition(position);

        FileInfo fileInfo;
        fileInfo = mList.get(position);

        if (holder.viewType == FrameworkConstant.ITEM_GRID) {
            setGridItemUI((Activity) context, holder, fileInfo);
        }
        else if(holder.viewType == FrameworkConstant.ITEM_LIST) {
            setListItemUI((Activity) context, holder);
        }

        //title and subtitle
        if(holder.viewType == FrameworkConstant.ITEM_GRID) {
            if (fileInfo.showTitleLayout) {  //圖片類型不顯示title
                holder.title_layout.setVisibility(View.VISIBLE);
                if (fileInfo.title != null)
                    holder.title.setText(fileInfo.title);
                else
                    holder.title.setText("");

                holder.default_title.setVisibility(View.GONE);
            } else {
                holder.title_layout.setVisibility(View.GONE);
                if (fileInfo.title != null)
                    holder.default_title.setText(fileInfo.title);
                else
                    holder.default_title.setText("");
            }
        }
        else{
            if (fileInfo.title != null) {
                holder.title.setText(fileInfo.title);
            } else {
                holder.title.setText("");
            }

            if (holder.subtitle != null && fileInfo.subtitle != null) {
                holder.subtitle.setText(fileInfo.subtitle);

                if (fileInfo.subtitle.equals("")){
                    holder.subtitle.setVisibility(View.GONE);
                    holder.title.setGravity(Gravity.CENTER_VERTICAL);
                }
            }
            else {
                if (holder.subtitle != null)
                    holder.subtitle.setVisibility(View.GONE);
                holder.title.setGravity(Gravity.CENTER_VERTICAL);
            }
        }

        //Start Thumbnail
        UnitConverter converter = new UnitConverter(context);
        int width = 0, height = 0;
        if(holder.viewType == FrameworkConstant.ITEM_GRID) {
            width = height = UiHelper.calculateGridItemWidth(context);
        }
        else {
            width = (int) converter.convertPixelToDp(72);
            height = (int) converter.convertPixelToDp(72);
        }
        Point thumbSize = new Point(width, height);

        //先探取cache裡有沒有圖檔
        Bitmap cachedResult = null;
        if (mThumbnailCallback != null)
            cachedResult = mThumbnailCallback.loadBitmapCache((fileInfo.path + ":ts" + thumbSize), holder.icon);
        //讀取default icon，若為-1則以未知檔案icon來呈現
        int resId = (fileInfo.defaultIconResourceId != -1 ? fileInfo.defaultIconResourceId : R.drawable.ic_filelist_unknow_grey);
        if (cachedResult != null) {  //有圖檔直接套用
            holder.icon.setImageBitmap(cachedResult);
            if (holder.default_layout != null)
                holder.default_layout.setVisibility(View.GONE);
        }
        else {    //沒有圖檔則觸發Thumbnail callback向Manager索取圖檔
            if (holder.viewType == FrameworkConstant.ITEM_LIST){
                holder.icon.setImageResource(resId);
            }
            else if (holder.viewType == FrameworkConstant.ITEM_GRID){
                holder.icon.setImageBitmap(null);
                holder.default_icon.setImageResource(resId);
                holder.default_layout.setVisibility(View.VISIBLE);
            }

            if (mThumbnailCallback != null) {
                mThumbnailCallback.loadThumbnail(context, fileInfo.path, position, holder.icon, fileInfo.type, thumbSize);
            }
        }
        //End of Thumbnail

        //ListView下的資料夾，要顯示往右的箭頭
        if(holder.viewType == FrameworkConstant.ITEM_LIST) {
            holder.info.setImageBitmap(null);

            if (fileInfo.defaultIconResourceId == R.drawable.ic_filelist_folder_grey)
                holder.info.setImageResource(R.drawable.ic_brower_listview_filearrow);
            else if (fileInfo.defaultIconResourceId == R.drawable.ic_usb_grey)
                holder.info.setImageResource(R.drawable.ic_exit_grey);
        }

        //isChecked mark
        holder.itemView.setSelected(fileInfo.isChecked);
        holder.mark.setVisibility(isSelectMode ? View.VISIBLE : View.INVISIBLE);
        if(fileInfo.isChecked) {
            holder.mark.setImageResource(R.drawable.ic_page_select_selected);
            holder.mark.setVisibility(View.VISIBLE);
        }
        else
            holder.mark.setImageResource(R.drawable.ic_page_select_empty);

        //grid的透明遮罩
        if(holder.viewType == FrameworkConstant.ITEM_GRID) {
            holder.icon_mask.setVisibility(fileInfo.isChecked ? View.VISIBLE : View.INVISIBLE);
        }
    }

    public static void setGridItemUI(Activity mActivity, RecyclerViewAdapter.ViewHolder holder, FileInfo fileInfo) {
        if(FrameworkConstant.mPortraitScreenWidth == 0 || FrameworkConstant.mPortraitScreenHeight == 0)
            UiHelper.updateScreenSize(mActivity);

        UnitConverter converter = new UnitConverter(mActivity);

        holder.title_layout.setBackgroundColor(Color.TRANSPARENT);
        holder.title_layout.getBackground().setAlpha(255);

        int px3 = (int) converter.convertPixelToDp(1.5f);
        holder.mark.setPadding(0, (int) converter.convertPixelToDp(6f), (int) converter.convertPixelToDp(6f), 0);

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.item_manager.getLayoutParams();
        lp.setMargins(px3, px3, px3, px3);
        holder.item_manager.setLayoutParams(lp);

        holder.item_manager.getLayoutParams().height = UiHelper.calculateGridItemWidth(mActivity);
        holder.title_layout.getLayoutParams().height = (int) converter.convertPixelToDp(50);
        holder.default_title.getLayoutParams().height = (int) converter.convertPixelToDp(50);
        holder.title.setTextSize(converter.convertPtToSp(28));
        holder.title.setTextColor(mActivity.getResources().getColor(R.color.c_02));
        holder.default_title.setTextSize(converter.convertPtToSp(28));
        holder.default_title.setTextColor(mActivity.getResources().getColor(R.color.c_02));

        if (fileInfo.mediaIconResourceId != -1){
            holder.title_layout.setBackgroundColor(mActivity.getResources().getColor(R.color.colorBlack));
            holder.title_layout.getBackground().setAlpha(123);

            holder.title_icon.setVisibility(View.VISIBLE);
            holder.title_icon.setImageResource(fileInfo.mediaIconResourceId);
            int px8 = (int) converter.convertPixelToDp(8);
            holder.title_icon.setPadding(px8,0,px8,0);
            holder.title.setTextColor(mActivity.getResources().getColor(R.color.colorWhite));
            if (fileInfo.mediaIconResourceId == FileInfo.mediaTypeTitleWithoutIcon)
                holder.title.setGravity(Gravity.CENTER);
            else {
                lp = (ViewGroup.MarginLayoutParams) holder.title.getLayoutParams();
                lp.setMargins(0, 0, (int) converter.convertPixelToDp(10), 0);
                holder.title.setLayoutParams(lp);
                holder.title.setGravity(Gravity.LEFT);
            }
        }
        else{
            holder.title_icon.setVisibility(View.GONE);
            holder.title.setGravity(Gravity.CENTER);
        }
    }


    public static void setListItemUI(Activity mActivity, RecyclerViewAdapter.ViewHolder holder){
        if(FrameworkConstant.mPortraitScreenWidth == 0 || FrameworkConstant.mPortraitScreenHeight == 0)
            UiHelper.updateScreenSize(mActivity);

        float multiple = 1.0f;
        if (UiHelper.isPad(mActivity)){
            multiple = 1.2f;
        }

        UnitConverter converter = new UnitConverter(mActivity);

        holder.item_manager.getLayoutParams().height = (int) converter.convertPixelToDp(120 * multiple);

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) holder.item_manager.getLayoutParams();
        lp.setMargins(0,(int) converter.convertPixelToDp(2.5f), 0, (int) converter.convertPixelToDp(2.5f));
        holder.item_manager.setLayoutParams(lp);

        holder.title.getLayoutParams().height = (int) converter.convertPixelToDp(30 * multiple);
        holder.subtitle.getLayoutParams().height = (int) converter.convertPixelToDp(30 * multiple);

        holder.title.setTextSize(converter.convertPtToSp(36 * multiple));
        holder.title.setTextColor(mActivity.getResources().getColor(R.color.c_02));
        holder.subtitle.setTextSize(converter.convertPtToSp(28 * multiple));
        holder.subtitle.setTextColor(mActivity.getResources().getColor(R.color.c_03));

        holder.icon.getLayoutParams().height = (int) converter.convertPixelToDp(72 * multiple);
        holder.icon.getLayoutParams().width = (int) converter.convertPixelToDp(72 * multiple);

        lp = (ViewGroup.MarginLayoutParams) holder.title_layout.getLayoutParams();
        lp.setMargins((int) converter.convertPixelToDp(36), 0, 0, 0);
        holder.title_layout.setLayoutParams(lp);

        lp = (ViewGroup.MarginLayoutParams) holder.icon_layout.getLayoutParams();
        lp.setMargins((int) converter.convertPixelToDp(20), 0, 0, 0);
        holder.icon_layout.setLayoutParams(lp);

        lp = (ViewGroup.MarginLayoutParams) holder.info.getLayoutParams();
        lp.setMargins(0, 0, (int) converter.convertPixelToDp(32), 0);
        holder.info.setLayoutParams(lp);
    }

    //List顯示item的數量，此處填多少就顯示多少
    @Override
    public int getItemCount() {
        if (mSection == null)
            return hasFooter() ? mList.size() + 1 : mList.size();
        else
            return hasFooter() ? mList.size() + mSection.size() + 1 : mList.size() + mSection.size();
    }

    //似乎是onCreateViewHolder裡的viewType?
    @Override
    public int getItemViewType(int position) {
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return 0;

        int layout_mode = MyPreference.getBrowserViewType(context, mType, FrameworkConstant.ITEM_LIST);
        if (isHeader(position))
            return FrameworkConstant.HEADER_ITEM;
        if (isFooter(position))
            return FrameworkConstant.FOOTER_ITEM;

        return layout_mode;
    }

    public boolean isHeader(int position) {
        if (mSection == null)
            return false;
        else{
            return mSection.containsKey(position);
        }
    }

    public boolean isFooter(int position) {
        if (mSection == null)
            return (hasFooter() && (position == mList.size()));
        else{
            return (hasFooter() && position == mList.size() + mSection.size());
        }
    }

    public boolean hasFooter() {
        return mList.size() > 0;
    }

    public void deselectAllItems(){
        if (mList == null)
            return;

        for (FileInfo file : mList)
            file.isChecked = false;

        selectedItemCount = 0;   //更新選取個數
    }

    public void selectAllItems(){
        if (mList == null)
            return;

        for (FileInfo file : mList)
            file.isChecked = true;

        selectedItemCount = mList.size();   //更新選取個數

        notifyDataSetChanged();
    }

    public boolean isSelectAll(){
        if (mList == null)
            return false;

        for (FileInfo file : mList)
            if (!file.isChecked)
                return false;

        return true;
    }

    public int getSelectedItemCount(){
        getSelectedFiles();
        return selectedItemCount;
    }

    protected ArrayList<FileInfo> getSelectedFiles(){
        ArrayList<FileInfo> selectedList = new ArrayList<>();
        if (mList == null)
            return selectedList;

        selectedItemCount = 0;
        for (FileInfo file : mList) {
            if (file.isChecked) {
                selectedList.add(file);
                selectedItemCount++;
            }
        }
        return selectedList;
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener, View.OnLongClickListener {

        public View itemView;
        public int viewType;

        public ImageView mark;  //被選取時的勾勾
        public View item_manager;  //整個item的layout
        public View title_layout;  //放title、subtitle(只有list)的地方
        public View icon_layout;   //(只在list) icon的地方
        public ImageView icon; //thumbnail
        public ImageView default_icon;
        public ImageView icon_mask;    //gridView的選取模糊效果
        public ImageView title_icon;
        public ImageView info;
        public TextView title;
        public TextView subtitle;
        public TextView default_title;  //預設圖的icon
        public RelativeLayout default_layout;

        public ViewHolder(View itemView, int viewType) {
            super(itemView);

            this.itemView = itemView;
            this.viewType = viewType;

            if (viewType == FrameworkConstant.HEADER_ITEM){
                title = (TextView) itemView.findViewById(R.id.section_text);
                return;
            }

            if (viewType == FrameworkConstant.FOOTER_ITEM)
                return;

            item_manager = (View) itemView.findViewById(R.id.item_manage);
            title_layout = (View)itemView.findViewById(R.id.item_title_layout);
            icon_layout = (View)itemView.findViewById(R.id.item_icon_layout);

            mark = (ImageView) itemView.findViewById(R.id.item_mark);
            title = (TextView) itemView.findViewById(R.id.item_title);

            if (viewType == FrameworkConstant.ITEM_LIST) {
                info = (ImageView) itemView.findViewById(R.id.item_info);
                subtitle = (TextView) itemView.findViewById(R.id.item_subtitle);
                icon = (ImageView) itemView.findViewById(R.id.item_icon);

                info.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(mRecyclerItemCallback != null) {
                            int position = getAdapterPosition();
                            if (position >= mList.size() || position < 0)
                                return;

                            if (mSection == null) {   //因為可能有分group的關係，position可能是錯的要修正
                                mRecyclerItemCallback.onRecyclerInfoClick(mList.get(position), position);
                            }
                            else {
                                int pos = positionToSectionPosition(position);
                                mRecyclerItemCallback.onRecyclerInfoClick(mList.get(pos), pos);
                            }
                        }
                    }
                });
            }
            else if (viewType == FrameworkConstant.ITEM_GRID) {
                icon_mask = (ImageView) itemView.findViewById(R.id.item_icon_mask);
                title_icon = (ImageView) itemView.findViewById(R.id.item_title_icon);
                default_title = (TextView) itemView.findViewById(R.id.item_default_title);
                default_icon = (ImageView) itemView.findViewById(R.id.item_default_icon);
                default_layout = (RelativeLayout) itemView.findViewById(R.id.default_layout);

                default_layout.setVisibility(View.VISIBLE);
                icon = (GridImageView) itemView.findViewById(R.id.item_icon);
                ((GridImageView) icon).setImageViewChangeListener(new GridImageView.OnImageChangeListiner() {
                    @Override
                    public void imageViewChanged(ImageView mImageView) {
                        default_layout.setVisibility(View.GONE);
                    }
                });
            }

            mark.setOnClickListener(this);

            itemView.setOnClickListener(this);
            itemView.setOnLongClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mRecyclerItemCallback != null) {
                int position = getAdapterPosition();
                if (mSection != null)   //因為可能有分group的關係，position可能是錯的要修正
                    position = positionToSectionPosition(position);
                if (position >= mList.size() || position < 0)
                    return;

                if (v == itemView)
                    mRecyclerItemCallback.onRecyclerItemClick(mList.get(position), position);
                else
                    mRecyclerItemCallback.onRecyclerThumbnailClick(mList.get(position), position);
            }
        }

        @Override
        public boolean onLongClick(View v) {
            if(mRecyclerItemCallback != null) {
                int position = getAdapterPosition();
                if (mSection != null)   //因為可能有分group的關係，position可能是錯的要修正
                    position = positionToSectionPosition(position);
                if (position >= mList.size() || position < 0)
                    return false;

                mRecyclerItemCallback.onRecyclerItemLongClick(mList.get(position), position);
            }
            return true;
        }

    }

    protected void updateDataList(ArrayList<FileInfo> list){
        mList = list;
    }

    protected void appendDataList(ArrayList<FileInfo> data){
        mList.addAll(data);
    }

    protected void updateGroupListAndSection(ArrayList<FileInfo> groupList, HashMap<Integer, String> section){
        mList = groupList;
        mSection = section;
    }

    protected void appendGroupListAndSection(ArrayList<FileInfo> groupList, HashMap<Integer, String> section){
        mList.addAll(groupList);
        mSection = section;
    }

    protected void setSelectMode(boolean enable){
        isSelectMode = enable;
        if (!isSelectMode){
            deselectAllItems();
        }
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder holder) {
        super.onViewRecycled(holder);
        mRecyclerItemCallback.onViewRecycled(holder);
    }
}