package com.transcend.browserframework.Browser;

import android.content.Context;
import android.content.res.Configuration;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UnitConverter;
import com.transcend.browserframework.Utils.FileGroup;
import com.transcend.browserframework.Utils.FileInfo;
import com.transcend.browserframework.Utils.MyPreference;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static android.widget.AbsListView.OnScrollListener.SCROLL_STATE_IDLE;


/**
 * Created by mike_chen on 2018/1/17.
 */

public class TabInfo{
    private String TAG = TabInfo.class.getSimpleName();

    //mList以列表方式紀錄，為mGroup列表化
    private ArrayList<FileInfo> mList = new ArrayList<>();

    private HashMap<Integer, String> mSection = null;
    private ArrayList<FileGroup> mGroup = new ArrayList<>();

    private int mType;  //紀錄現在的類型(AllFile, Image, Music, Video, Doc...)
    private int iconId; //Tab上的圖示
    private WeakReference<Context> mContext;

    private View mRootView; //顯示list的容器
    private ImageView mEmpty;    //沒有檔案時的頁面
    private RecyclerView mRecyclerView;
    private RecyclerViewAdapter mRecyclerAdapter;
    private View mLoadingContainer;   //讀取中的圖示
    private View mListContainer;    //list

    private GridLayoutManager mLayout;
    private int mColumnCount = 1;  // This will get updated when layout changes.
    private int mViewType;  //List or Grid

    private boolean showGroup;

    private OnRecyclerViewScrollListener mRecyclerListener;
    public interface OnRecyclerViewScrollListener{
        void RecyclerViewScrollBottom();
        void RecyclerViewScrollStateChanged(RecyclerView recyclerView, int newState);
        //newState:
        //public static final int SCROLL_STATE_IDLE = 0;    // 靜止,沒有滾動
        //public static final int SCROLL_STATE_DRAGGING = 1;    //正在被外部拖拽,一般為用戶正在用手指滾動
        //public static final int SCROLL_STATE_SETTLING = 2;    //自動滾動開始
    }
    public void setOnRecyclerScrollListener(OnRecyclerViewScrollListener listener){
        mRecyclerListener = listener;
    }

    public TabInfo(int type, int icon_id, boolean show_group, Context context) {
        mType = type;
        iconId = icon_id;
        showGroup = show_group;
        mContext = new WeakReference<Context>(context);
        mViewType = MyPreference.getBrowserViewType(context, mType, FrameworkConstant.ITEM_LIST);

        LayoutInflater mInflater = LayoutInflater.from(context);
        build(mInflater);
    }

    private View build(LayoutInflater inflater) {
        if(mRootView!=null)
            return mRootView;

        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return null;

        mRootView = inflater.inflate(R.layout.pager_layout, null);
        mEmpty = mRootView.findViewById(R.id.empty_view);

        mRecyclerView = (RecyclerView) mRootView.findViewById(R.id.recycler_view);
        mRecyclerAdapter = new RecyclerViewAdapter(context, mType);
        mRecyclerView.setAdapter(mRecyclerAdapter);

        mColumnCount = calculateColumnCount(mViewType);
        mLayout = new GridLayoutManager(context, mColumnCount);
        mLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {  //某item占據的列數
            @Override
            public int getSpanSize(int position) {
                if (mRecyclerAdapter.isFooter(position))
                    return mLayout.getSpanCount();
                else{
                    return 1;
                }
            }
        });
        mLayout.setSpanCount(mColumnCount);
        mRecyclerView.setLayoutManager(mLayout);

        mRecyclerView.setItemAnimator(new DefaultItemAnimator());

        mLoadingContainer = mRootView.findViewById(R.id.loading_container);
        mListContainer = mRootView.findViewById(R.id.list_container);

        //監聽recycler view滑動
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                //super.onScrollStateChanged(recyclerView, newState);
                mRecyclerListener.RecyclerViewScrollStateChanged(recyclerView, newState);
                if (!recyclerView.canScrollVertically(1)) {
                    if (newState == SCROLL_STATE_IDLE)
                        mRecyclerListener.RecyclerViewScrollBottom();
                }
            }
        });

        return mRootView;
    }

    //更新內容
    public void updateTabData(ArrayList<FileInfo> items) {
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        mList = items;
        mGroup = groupUpList(mList);
        mSection = new HashMap<>();

        if((mGroup != null && mGroup.size()!=0) && showGroup)
            setGroupAdapter(); //設置群組
        else {
            mRecyclerView.setAdapter(mRecyclerAdapter);

            mColumnCount = calculateColumnCount(mViewType);
            mLayout = new GridLayoutManager(context, mColumnCount);
            mLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {  //某item占據的列數
                @Override
                public int getSpanSize(int position) {
                    if (mRecyclerAdapter.isFooter(position))
                        return mLayout.getSpanCount();
                    else{
                        return 1;
                    }
                }
            });
            mLayout.setSpanCount(mColumnCount);
            mRecyclerView.setLayoutManager(mLayout);
        }

        mRecyclerAdapter.updateDataList(mList);

        showLoadingResult(isListEmpty());
        mRecyclerAdapter.notifyDataSetChanged();
    }

    //將List轉為Group
    private ArrayList<FileGroup> groupUpList(ArrayList<FileInfo> list){
        if (!showGroup)
            return null;
        if (list == null || list.size() == 0)
            return null;

        ArrayList<FileGroup> resultGroup = new ArrayList<>();
        for(FileInfo file : list){
            boolean isMatch = false;

            if (file.groupTitle == null)
                file.groupTitle = "";

            for(FileGroup group : resultGroup){
                if(group.title.equals(file.groupTitle)) {
                    group.addFile(file);
                    isMatch = true;
                    break;
                }
            }
            if (!isMatch){  //若沒有相符合的標題，則創造一個
                FileGroup group = new FileGroup(file.groupTitle);
                group.addFile(file);
                resultGroup.add(group);
            }
        }

        return resultGroup;
    }

    private void setGroupAdapter(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        //記錄Sections
        int position = 0;
        for(FileGroup group : mGroup){
            if(!group.title.equals("")) {
                UnitConverter converter = new UnitConverter(context);
                mSection.put(position, converter.parseDate(group.title));
                position += group.getListSize() + 1;
            }
        }

        //重構List順序以符合Group順序，防呆: 以免丟進來的List未經排序而產生錯誤
        ArrayList<FileInfo> list = new ArrayList<>();
        for (FileGroup group : mGroup){
            for(FileInfo file : group.getList()){
                list.add(file);
            }
        }

        mList = list;

        //更新adapter
        mRecyclerAdapter.updateGroupListAndSection(mList, mSection);

        mColumnCount = calculateColumnCount(mViewType);
        mLayout = new GridLayoutManager(context, mColumnCount);
        mLayout.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {  //某item占據的列數
            @Override
            public int getSpanSize(int position) {
                if (mSection.containsKey(position)){
                    return mColumnCount;
                }
                else if (mRecyclerAdapter.isFooter(position))
                    return mLayout.getSpanCount();
                else{
                    return 1;
                }
            }
        });
        mLayout.setSpanCount(mColumnCount);
        mRecyclerView.setLayoutManager(mLayout);
    }

    private void appendGroupItems(ArrayList<FileInfo> list){
        //記錄目前大小，以便在最後的位置插入新增資料
        int preDataSize = mList.size();
        int preSectionSize = mSection.size();

        ArrayList<FileGroup> resultGroup = groupUpList(list);
        mGroup.addAll(resultGroup);

        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        mSection = new HashMap<>();

        //記錄Sections
        int position = 0;
        for(FileGroup group : mGroup){
            if(!group.title.equals("")) {
                UnitConverter converter = new UnitConverter(context);
                mSection.put(position, converter.parseDate(group.title));
                position += group.getListSize() + 1;
            }
        }

        //重組List順序以符合Group順序
        ArrayList<FileInfo> newList = new ArrayList<>();
        for (FileGroup group : resultGroup){
            for(FileInfo file : group.getList()){
                newList.add(file);
            }
        }
        //mGroupList.addAll(newList);

        mRecyclerAdapter.appendGroupListAndSection(newList, mSection);

        //插入最後
        mRecyclerAdapter.notifyItemInserted(preDataSize + preSectionSize);

        //更新
        mRecyclerAdapter.notifyItemRangeChanged(preDataSize + preSectionSize, newList.size() + (mSection.size()-preSectionSize));
    }

    private boolean isListEmpty() {
        return mList == null || mList.isEmpty();
    }

    protected void setEmptyImage(int resId) {//when the list is empty
        mEmpty.setImageResource(resId);
    }

    protected void showLoadingResult(boolean empty) {
        if (empty) {
            mListContainer.setVisibility(View.GONE);
            mEmpty.setVisibility(View.VISIBLE);
        } else {
            mListContainer.setVisibility(View.VISIBLE);
            mEmpty.setVisibility(View.GONE);
        }
    }

    protected void setLoadingProgressVisibility(int visible){
        if (mLoadingContainer != null)
            mLoadingContainer.setVisibility(visible);
    }

    //listView, gridView 之間的轉換更新
    public void updateLayoutType(int viewType) {
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        MyPreference.setBrowserViewType(context, mType, viewType);
        mViewType = viewType;

        mColumnCount = calculateColumnCount(viewType);
        if (mLayout != null) {
            mLayout.setSpanCount(mColumnCount);
            mRecyclerView.setLayoutManager(mLayout);
        }

        if (mRecyclerAdapter == null || mRecyclerView == null)
            return;
        mRecyclerAdapter.setViewType(mViewType);    //在此更新顯示mode比較保險
        mRecyclerView.requestLayout();
    }

    private int calculateColumnCount(int viewType) {
        if (viewType == FrameworkConstant.ITEM_LIST) {
            // List mode is a "grid" with 1 column.
            return 1;
        }

        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務

            if(UiHelper.isPad(context)){
                if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                    return 8;
                else
                    return 6;
            }
            else{
                if(context.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
                    return 6;
                else
                    return 3;
            }
        } else {
            return 3;
        }
    }

    public ArrayList<FileGroup> getFileGroupList(){
        return mGroup;
    }

    public ArrayList<FileInfo> getFileList(){
        return mList;
    }

    protected void appendData(ArrayList<FileInfo> data){
        if (!showGroup) {
            int currentSize = mList.size();

            mRecyclerAdapter.appendDataList(data);

            //插入最後
            mRecyclerAdapter.notifyItemInserted(currentSize);

            //更新
            mRecyclerAdapter.notifyItemRangeChanged(currentSize, data.size());
        } else {
            appendGroupItems(data);
        }
    }

    public void deselectAll(){
        mRecyclerAdapter.deselectAllItems();
    }

    public void selectAll(){
        mRecyclerAdapter.selectAllItems();
    }

    public ArrayList<FileInfo> getSelectedFiles(){
        return mRecyclerAdapter.getSelectedFiles();
    }

    public boolean isSelectAll(){
        return mRecyclerAdapter.isSelectAll();
    }

    public RecyclerViewAdapter getAdapter() {
        return mRecyclerAdapter;
    }

    public RecyclerView getRecyclerView() {
        return mRecyclerView;
    }

    public int getIconId(){
        return iconId;
    }

    public int getType(){
        return mType;
    }

    public ArrayList<FileInfo> getFileListWithType(int type){
        ArrayList<FileInfo> list = new ArrayList<FileInfo>();
        if (mList == null)
            return list;

        for (FileInfo f : mList) {
            if (f.type == type)
                list.add(f);
        }
        return list;
    }

    public int getSelectedItemCount(){
        return mRecyclerAdapter.getSelectedItemCount();
    }

    public View getRootView(){
        return mRootView;
    }
}