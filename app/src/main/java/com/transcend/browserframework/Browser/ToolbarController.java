package com.transcend.browserframework.Browser;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatImageView;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UnitConverter;

public class ToolbarController implements TextWatcher {

    //自定義Toolbar
    private Toolbar mToolbar;
    private ImageView toggleBtn, searchBtn, searchClearBtn;
    private EditText searchEditText;    //Search mode 輸入
    private LinearLayout dropdownLayout;    // 存放history drop down
    private TextView mToolbarTitle; //Home等非Browser用Title
    private RelativeLayout searchModeLayout;    //存放searchEditText、searchClearBtn

    private AppCompatSpinner mDropdown; //history page
    private DropDownAdapter mDropdownAdapter;   //history page
    private ImageView historyPage_arrow;   //history page arrow

    private AppCompatActivity mActivity;

    /**
     * ToolbarMode description:
     * 1.NORMAL                            menu_icon, middle_title
     * 2.NORMAL_DROPDOWN      menu_icon, dropdown
     * 3.BACK                                  back_icon, middle_title
     * 4.BACK_DROPDOWN            back_icon, dropdown
     * 5.BACK_LEFT                        back_icon, left_title
     * 6.CLOSE                                close_icon, middle_title
     * 7.CLOSE_DROPDOWN          close_icon, dropdown
     * 8.BROWSER                           menu_icon, dropdown, search_icon
     * 9.SEARCH                              back_icon, search_layout
     * 10.TITLE                                middle_title
     */
    public enum ToolbarMode {
        NORMAL, NORMAL_DROPDOWN, BACK, BACK_DROPDOWN, BACK_LEFT, CLOSE, CLOSE_DROPDOWN, BROWSER, SEARCH, TITLE
    }
    private ToolbarMode mToolbarMode;

    private enum ToggleStyle{    //依序為: Menu圖示、關閉圖示、返回圖示、隱藏
        Menu, Close, Back, Nothing
    }

    private enum TitleStyle{    //依序為: 下拉式選單、置中標題、置左標題、隱藏
        DropDown, Middle, Left, Gone
    }

    private enum SearchStyle{   //依序為: 搜尋中、一般顯示、隱藏
        OnWork, NoWork, Gone
    }

    private OnToolbarItemClick mToolbarItemListener;
    public interface OnToolbarItemClick{
        void OnToggleClick();
        void OnSearchModeSubmitClick(String SearchText);
        void OnSearchModeEditTextChange(String SearchText);
        void OnSearchModeStart();
        void OnSearchModeExit();
    }
    public void setOnToolbarItemListener(OnToolbarItemClick listener){
        mToolbarItemListener = listener;
    }

    public ToolbarController(AppCompatActivity activity){
        mActivity = activity;
    }

    //需要在activity設置layout後才能呼叫，不然會因為抓不到.toolbar而掛掉
    private void initToolbarView(){
        mToolbar = (Toolbar) mActivity.findViewById(R.id.toolbar);
        if(mToolbar == null) {
            return;
        }

        toggleBtn = (ImageView) mActivity.findViewById(R.id.toggle_btn);
        searchBtn = (ImageView) mActivity.findViewById(R.id.search_btn);
        searchClearBtn = (ImageView) mActivity.findViewById(R.id.search_clear);
        dropdownLayout = (LinearLayout) mActivity.findViewById(R.id.toolbar_dropdown_layout);
        searchEditText = (EditText) mActivity.findViewById(R.id.search_editText);
        mToolbarTitle = (TextView) mActivity.findViewById(R.id.toolbar_title);
        searchModeLayout = (RelativeLayout) mActivity.findViewById(R.id.searchMode_layout);

        mDropdown = (AppCompatSpinner) mActivity.findViewById(R.id.main_dropdown);
        historyPage_arrow = (ImageView) mActivity.findViewById(R.id.dropdown_arrow);
        initHistoryPageDropdown();

        toggleBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mToolbarMode == ToolbarMode.SEARCH) {    //若目前已是搜尋模式，表示要離開搜尋模式
                    toolbar_browserMode();
                    mToolbarItemListener.OnSearchModeExit();
                } else
                    mToolbarItemListener.OnToggleClick();
            }
        });

        searchBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                toolbar_searchMode();
            }
        });

        searchClearBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                searchEditText.setText("");
            }
        });

        UnitConverter converter = new UnitConverter(mActivity);
        searchEditText.getLayoutParams().height = (int) converter.convertPixelToDp(56);
        //監聽keyboard submit
        searchEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if(actionId== EditorInfo.IME_ACTION_DONE){
                    mToolbarItemListener.OnSearchModeSubmitClick(v.getText().toString());
                }
                return false;
            }
        });

        mToolbarTitle.setTextSize(converter.convertPtToSp(42));

        mActivity.setSupportActionBar(mToolbar);
        mActivity.getSupportActionBar().setDisplayShowTitleEnabled(false);    // disable the default title element here (for centered title)
        mActivity.getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        mActivity.getSupportActionBar().setDisplayShowHomeEnabled(false);  // show or hide the default home button
        mActivity.getSupportActionBar().setDisplayShowCustomEnabled(true);    // enable overriding the default toolbar layout
    }

    //初始設置History page
    private void initHistoryPageDropdown(){
        if (mDropdown == null)
            return;

        mDropdownAdapter = new DropDownAdapter(mActivity, historyPage_arrow);
        mDropdown.setAdapter(mDropdownAdapter);
        mDropdown.setDropDownVerticalOffset(10);

        //更新後才能顯示dropdown spinner上的文字
        mDropdownAdapter.updateDropDownList(FrameworkConstant.ROOT_PATH);
        mDropdownAdapter.notifyDataSetChanged();

        historyPage_arrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mDropdown.performClick();
            }
        });
    }

    //只有 Menu圖示 與 置中標題
    public void toolbar_normalMode(){
        setToggleStyle(ToggleStyle.Menu);
        setTitleStyle(TitleStyle.Middle);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.NORMAL;
    }

    //只有 Menu圖示 與 下拉式選單
    public void toolbar_normalDropDownMode(){
        setToggleStyle(ToggleStyle.Menu);
        setTitleStyle(TitleStyle.DropDown);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.NORMAL_DROPDOWN;
    }

    //只有 返回鍵圖示 與 置中標題
    public void toolbar_backMode(){
        setToggleStyle(ToggleStyle.Back);
        setTitleStyle(TitleStyle.Middle);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.BACK;
    }

    //只有 返回鍵圖示 與 下拉式選單
    public void toolbar_backDropDownMode(){
        setToggleStyle(ToggleStyle.Back);
        setTitleStyle(TitleStyle.DropDown);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.BACK_DROPDOWN;
    }

    //只有 返回鍵圖示 與 置左標題
    public void toolbar_backLeftMode(){
        setToggleStyle(ToggleStyle.Back);
        setTitleStyle(TitleStyle.Left);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.BACK_LEFT;
    }

    //只有 關閉圖示 與 置中標題
    public void toolbar_closeMode(){
        setToggleStyle(ToggleStyle.Close);
        setTitleStyle(TitleStyle.Middle);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.CLOSE;
    }

    //只有 關閉圖示 與 下拉式選單
    public void toolbar_closeDropDownMode(){
        setToggleStyle(ToggleStyle.Close);
        setTitleStyle(TitleStyle.DropDown);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.CLOSE_DROPDOWN;
    }

    //Menu圖示 、 搜尋圖示 與 下拉式選單
    public void toolbar_browserMode(){
        searchEditText.removeTextChangedListener(this); //先取消監聽不然關掉時會再搜尋一次

        setToggleStyle(ToggleStyle.Menu);
        setTitleStyle(TitleStyle.DropDown);
        setSearchStyle(SearchStyle.NoWork);

        searchEditText.setText("");
        //收起鍵盤
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(searchEditText.getWindowToken(), 0);

        mToolbarMode = ToolbarMode.BROWSER;
    }

    //返回圖示、無標題
    public void toolbar_searchMode(){
        setToggleStyle(ToggleStyle.Back);
        setTitleStyle(TitleStyle.Gone);
        setSearchStyle(SearchStyle.OnWork);

        searchEditText.requestFocus();
        mToolbarItemListener.OnSearchModeStart();
        searchEditText.addTextChangedListener(this);    //開始監聽文字更改
        //展開鍵盤
        InputMethodManager imm = (InputMethodManager) mActivity.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.showSoftInput(searchEditText, InputMethodManager.SHOW_IMPLICIT);

        mToolbarMode = ToolbarMode.SEARCH;
    }

    //只有 title
    public void toolbar_titleMode(){
        setToggleStyle(ToggleStyle.Nothing);
        setTitleStyle(TitleStyle.Middle);
        setSearchStyle(SearchStyle.Gone);

        mToolbarMode = ToolbarMode.TITLE;
    }

    private void setToggleStyle(ToggleStyle style){
        if (toggleBtn == null)
            initToolbarView();
        toggleBtn.setVisibility(View.VISIBLE);
        switch (style){
            case Menu:
                toggleBtn.setImageResource(R.drawable.ic_toggle_menu);
                break;
            case Back:
                toggleBtn.setImageResource(R.drawable.ic_back);
                break;
            case Close:
                toggleBtn.setImageResource(R.drawable.ic_navi_cancel_white);
                break;
            default:    //Nothing
                toggleBtn.setVisibility(View.GONE);
                break;
        }
    }

    private void setTitleStyle(TitleStyle style){
        if (toggleBtn == null)
            initToolbarView();
        mToolbarTitle.setVisibility(View.GONE); //中間的標題物件
        dropdownLayout.setVisibility(View.VISIBLE); //下拉式選單
        switch (style){
            case Left:
                historyPage_arrow.setVisibility(View.GONE);
                break;
            case DropDown:
                historyPage_arrow.setVisibility(View.VISIBLE);
                break;
            case Gone:
                dropdownLayout.setVisibility(View.GONE);
                break;
            default:    //Middle
                mToolbarTitle.setVisibility(View.VISIBLE);
                dropdownLayout.setVisibility(View.GONE);
                break;
        }
    }

    private void setSearchStyle(SearchStyle style){
        if (toggleBtn == null)
            initToolbarView();
        searchClearBtn.setVisibility(View.GONE);
        searchBtn.setVisibility(View.GONE);
        searchModeLayout.setVisibility(View.GONE);
        switch (style){
            case OnWork:
                searchModeLayout.setVisibility(View.VISIBLE);
                break;
            case NoWork:
                searchBtn.setVisibility(View.VISIBLE);
                break;
            default:    //GONE
                break;
        }
    }

    //給予權限來新增自己需求的adapter
    public void setDropDownAdapter(SpinnerAdapter adapter){
        if (mDropdown == null)
            initToolbarView();
        mDropdown.setAdapter(adapter);
    }

    //開啟/關閉dropdown功能
    public void enableDropDown(boolean enable){
        if (mDropdown == null)
            initToolbarView();
        mDropdown.setEnabled(enable);
        if (enable) //不給按就不要顯示下拉箭頭
            historyPage_arrow.setVisibility(View.VISIBLE);
        else
            historyPage_arrow.setVisibility(View.INVISIBLE);
    }

    //設定Toolbar標題
    public void setToolbarTitle(String text){
        if (mToolbarTitle == null)
            initToolbarView();
        mToolbarTitle.setText(text);
    }

    public String getToolbarTitle(){
        return mToolbarTitle.getText().toString();
    }

    public String getDropDownTopText(){
        return mDropdownAdapter.getTopText();
    }

    public void setOnDropdownItemSelectedListener(DropDownAdapter.OnDropdownItemSelectedListener listener){
        if (mDropdown == null)
            initToolbarView();
        if (mDropdownAdapter != null)
            mDropdownAdapter.setOnDropdownItemSelectedListener(listener);
    }

    //更新Dropdown內容，需為路徑模式(/storage/DCIM)
    public void updateDropDownList(String path){
        if (mDropdown == null)
            initToolbarView();
        if (mDropdownAdapter != null) {
            mDropdownAdapter.updateDropDownList(path);
            mDropdownAdapter.notifyDataSetChanged();
        }
    }

    public String getPathAtPosition(int position){
        if (mDropdown == null)
            initToolbarView();
        if (mDropdownAdapter != null)
            return mDropdownAdapter.getPath(position);
        else
            return "";
    }

    public void setMainPageTitle(String text){
        if (mDropdown == null)
            initToolbarView();
        if (mDropdownAdapter != null) {
            mDropdownAdapter.setMainPage(text);
        }
    }

    public Toolbar getToolbar(){
        if (mToolbar == null)
            initToolbarView();
        return mToolbar;
    }

    public ToolbarMode getCurrentToolbarMode(){
        return mToolbarMode;
    }

    public void setTitlePendingWithMenuItems(int items){
        UnitConverter converter = new UnitConverter(mActivity);
        int one_step = (int) converter.convertPixelToDp(96);
        mToolbarTitle.setPadding(one_step * items, 0, 0, 0);
    }

    public void setTitlePending(int left, int right){
        mToolbarTitle.setPadding(left, 0, right, 0);
    }

    public int getTitlePendingValue(){
        UnitConverter converter = new UnitConverter(mActivity);
        return (int) converter.convertPixelToDp(96);
    }

    public void setToolbarVisibility(int visibility){
        if (mToolbar == null)
            initToolbarView();

        if (visibility == View.INVISIBLE || visibility == View.VISIBLE || visibility == View.GONE)
            mToolbar.setVisibility(visibility);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        mToolbarItemListener.OnSearchModeEditTextChange(s.toString());
        if(s.length() == 0)
            searchClearBtn.setVisibility(View.GONE);
        else{
            searchClearBtn.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void afterTextChanged(Editable s) {
    }
}