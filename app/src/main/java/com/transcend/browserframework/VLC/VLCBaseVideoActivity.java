package com.transcend.browserframework.VLC;

import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.SingleView.VideoBaseActivity;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.PlayerUtils;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

import static android.view.View.GONE;
import static com.transcend.browserframework.VLC.VLCPlayer.PlayerState_ReachEnd;

/**
 * Created by mike_chen on 2018/1/31.
 */

public class VLCBaseVideoActivity extends AppCompatActivity{
    private final static String TAG = VLCBaseVideoActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private VLCPlayer vlc_player;
    private Handler mHandler = new Handler();

    private RelativeLayout control_layout;  //上一首、播放、下一首的layout，方便全螢幕時隱藏
    private ImageView play, prev, next, prev15, next15, delete, download;
    private SeekBar mSeekbar;
    private TextView current_time, duration_time;    //現在時間；總時間

    //監控螢幕方向變化
    private WindowManager mWindowManager = null;
    private int mLastRotation = -1;

    private Handler handler = new Handler();
    protected boolean curControllerHide;

    private SurfaceView mSurfaceView;
    private ImageView mVideoImage;  //影片預覽圖
    protected int mScreenW, mScreenH;
    protected boolean hideControlLayout = false;

    private ProgressBar mLoadingProgress;
    private RelativeLayout progressView;    //Loading 圖示

    private static int Navigation_bar_height = 0;
    private static int Status_bar_height = 0;
    private static int Notch_height = 0;

    private OnVideoViewControllerListener videoViewControllerListener;
    public interface OnVideoViewControllerListener{
        void OnEndReached();
        void OnNextButtonPress();
        void OnPreviousButtonPress();
        void OnDownloadButtonPress();
        void OnDeleteButtonPress();
    }
    public void setOnVideoViewControllerListener(OnVideoViewControllerListener listener){
        videoViewControllerListener = listener;
    }

    public void setVideoContentView() {
        //將UI的處理規劃另外作，再回傳給Activity
        LayoutInflater mInflater = LayoutInflater.from(this);
        View view = UiHelper.setVideoUiDimensioning(this, mInflater);
        setContentView(view);

        //避免沒更新到螢幕大小
        if(FrameworkConstant.mPortraitScreenWidth == 0 || FrameworkConstant.mPortraitScreenHeight == 0)
            UiHelper.updateScreenSize(this);

        Navigation_bar_height = UiHelper.getNavigationBarHeight(this);
        Status_bar_height = UiHelper.getStatusBarHeight(this);
        Notch_height = UiHelper.getNotchHeight(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            mScreenW = FrameworkConstant.mPortraitScreenWidth;
            mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
        }
        else{
            mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
            mScreenH = FrameworkConstant.mPortraitScreenWidth;
        }

        iniView();
        iniToolbar();
        initPlayer();
        setOrientationListener();

        progressView = (RelativeLayout) findViewById(R.id.video_progress_view);
        mLoadingProgress = findViewById(R.id.loading_progress);
        mLoadingProgress.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_style));

        //System bar 透明化
        //UiHelper.setSystemBarTranslucent(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorBlack));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBlack));
        }
        UiHelper.showBothSystemBarWithoutTranslucent(this);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            adjustLandscapeUi();
        }
        else{
            adjustPortraitUi();
        }
    }

    private void iniView(){
        control_layout = (RelativeLayout) findViewById(R.id.control_layout);
        play = (ImageView) findViewById(R.id.video_play);
        prev = (ImageView) findViewById(R.id.video_previous);
        next = (ImageView) findViewById(R.id.video_next);
        prev15 = (ImageView) findViewById(R.id.video_previous15);
        next15 = (ImageView) findViewById(R.id.video_next15);
        delete = (ImageView) findViewById(R.id.video_delete);
        download = (ImageView) findViewById(R.id.video_download);

        mSeekbar = (SeekBar) findViewById(R.id.video_seekbar);
        mSeekbar.setOnSeekBarChangeListener(OnSeekBarChangeListener);   //設置Listener

        current_time = (TextView) findViewById(R.id.video_current_time);
        duration_time = (TextView) findViewById(R.id.video_duration);

        mVideoImage = (ImageView) findViewById(R.id.video_image);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vlc_player.isPlaying())
                    pause();
                else
                    play();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoImage.setVisibility(View.VISIBLE);
                clearSurfaceView();
                if (vlc_player.isPlaying())
                    stop();
                videoViewControllerListener.OnNextButtonPress();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mVideoImage.setVisibility(View.VISIBLE);
                clearSurfaceView();
                if (vlc_player.isPlaying())
                    stop();
                videoViewControllerListener.OnPreviousButtonPress();
            }
        });

        prev15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long time = vlc_player.getTime() - 15000;
                if (time < 0)
                    time = 0;
                vlc_player.seekTo(time);
            }
        });

        next15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                long time = vlc_player.getTime() + 15000;
                if (time > vlc_player.getLength())
                    time = vlc_player.getLength();
                vlc_player.seekTo(time);
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.OnDeleteButtonPress();
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.OnDownloadButtonPress();
            }
        });

        mSurfaceView = (SurfaceView) findViewById(R.id.surface_view);
        mSurfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideControlLayout = !hideControlLayout;
                hideControllers(hideControlLayout);
            }
        });
    }

    private void iniToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.video_header_bar);
        mToolbarTitle = (TextView) findViewById(R.id.video_toolbar_title);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        UnitConverter converter = new UnitConverter(this);
        mToolbarTitle.setTextSize(converter.convertPtToSp(34));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //全螢幕後避免actionbar被statusbar擋住
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
        lp.setMargins(0, Status_bar_height, 0, 0);
        mToolbar.setLayoutParams(lp);
    }

    private void initPlayer(){
        vlc_player = new VLCPlayer(this);
    }

    protected void setupVideo(String url, boolean isRemote){
        mSeekbar.setProgress(0);
        duration_time.setText("00:00");
        current_time.setText("00:00");

        vlc_player.setDataSource(url, isRemote);
        vlc_player.setDisplay(mSurfaceView, mSurfaceView.getHolder());
    }

    protected void startUpdatingProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 500);
    }

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = vlc_player.getLength();
            long currentDuration = vlc_player.getTime();

            if (mSeekbar.getProgress() == 100){
                stop();
                mVideoImage.setVisibility(View.VISIBLE);
                clearSurfaceView();

                videoViewControllerListener.OnEndReached();
                return;
            }

            if (vlc_player.getPlayerState() == PlayerState_ReachEnd){   //目前VLC不會播到最後，會在前一刻N毫秒前停止
                updateTimeText(totalDuration, totalDuration);
                mSeekbar.setProgress(100);
            }
            else {
                // Updating progress bar
                int progress = (int) (PlayerUtils.getProgressPercentage(currentDuration, totalDuration));
                mSeekbar.setProgress(progress);
                updateTimeText(currentDuration, totalDuration);
            }

            // Running this thread after 200 milliseconds
            mHandler.postDelayed(this, 200);
        }
    };

    private void updateTimeText(long current, long duration){
        String durationText = PlayerUtils.milliSecondsToTimer(duration);
        // Displaying Total Duration time
        duration_time.setText(durationText);

        // Displaying time completed playing
        if (durationText.length() == "00:00:00".length())
            current_time.setText(PlayerUtils.milliSecondsToTimer(current, true));
        else
            current_time.setText(PlayerUtils.milliSecondsToTimer(current, false));
    }

    private void clearSurfaceView(){
        Canvas canvas = mSurfaceView.getHolder().lockCanvas();
        if (canvas == null)
            return;
        canvas.drawColor(Color.TRANSPARENT, PorterDuff.Mode.CLEAR);
        mSurfaceView.getHolder().unlockCanvasAndPost(canvas);
    }

    private void play(){
        vlc_player.start();
        startUpdatingProgressBar();
        play.setImageResource(R.drawable.ic_pause);

        mVideoImage.setVisibility(View.GONE);

        setLoadingProgressVisibility(GONE);
    }

    private void pause(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.pause();
        play.setImageResource(R.drawable.ic_play);
    }

    private void stop(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.stop();
        play.setImageResource(R.drawable.ic_play);
        mSeekbar.setProgress(0);
    }

    public void hideControllers(boolean hide){
        if(hide){
            control_layout.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);

            //隱藏System bar (Navigation bar, status bar)
            UiHelper.hideBothSystemBarWithoutTranslucent(this);
            curControllerHide = true;
        }
        else{
            control_layout.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);

            //隱藏Navigation bar (Back, Home, Apps)
            UiHelper.showBothSystemBarWithoutTranslucent(this);
            curControllerHide = false;
        }
    }

    public void setAutoHideControllerUI(int millisecond){
        handler.postDelayed(prepareToHideControllerUI, millisecond);
    }

    public void cancelAutoHideControllerUI(){
        handler.removeCallbacks(prepareToHideControllerUI);
    }

    private Runnable prepareToHideControllerUI = new Runnable() {
        @Override
        public void run() {
            hideControllers(true);
        }
    };

    public Toolbar getToolbar(){
        return mToolbar;
    }

    public void setToolbarTitle(String text){
        mToolbarTitle.setText(text);
    }

    protected void enablePrevBtn(boolean enable){
        prev.setEnabled(enable);
        if (enable)
            prev.setImageResource(R.drawable.ic_prev);
        else
            prev.setImageResource(R.drawable.ic_singleview_playback_s_grey);
    }

    protected void enableNextBtn(boolean enable){
        next.setEnabled(enable);
        if (enable)
            next.setImageResource(R.drawable.ic_next);
        else
            next.setImageResource(R.drawable.ic_singleview_playnext_s_grey);
    }

    public ImageView getVideoImageView(){
        return mVideoImage;
    }

    public void setVideoImage(Bitmap bitmap){
        mVideoImage.setImageBitmap(bitmap);
        mVideoImage.setVisibility(View.VISIBLE);
        mVideoImage.getLayoutParams().width = mScreenW;
        mVideoImage.getLayoutParams().height = mScreenH;
    }

    //設置ToolbarTitle是否為singleLine(For DPB)
    public void setToolbarTitleIsSingleLine(boolean enable){
        mToolbarTitle.setSingleLine(enable);
    }

    public void setLoadingProgressVisibility(int visible){
        if (visible == View.VISIBLE || visible == View.GONE || visible == View.INVISIBLE)
            progressView.setVisibility(visible);
    }

    protected void setOrientationListener(){
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        OrientationEventListener orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {

                Display display = mWindowManager.getDefaultDisplay();
                int rotation = display.getRotation();
                if ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && rotation != mLastRotation) {
                    Status_bar_height = UiHelper.getStatusBarHeight(VLCBaseVideoActivity.this);
                    Notch_height = UiHelper.getNotchHeight(VLCBaseVideoActivity.this);

                    adjustLandscapeUi();

                    mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
                    mScreenH = FrameworkConstant.mPortraitScreenWidth;
                    mLastRotation = rotation;

                    adjust15sController();
                }
                else if (rotation == Surface.ROTATION_0 && mLastRotation != 0){
                    Status_bar_height = UiHelper.getStatusBarHeight(VLCBaseVideoActivity.this);
                    Notch_height = UiHelper.getNotchHeight(VLCBaseVideoActivity.this);

                    adjustPortraitUi();

                    mScreenW = FrameworkConstant.mPortraitScreenWidth;
                    mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
                    mLastRotation = rotation;

                    adjust15sController();
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        }
    }

    protected void adjustLandscapeUi(){
        if (UiHelper.isPad(this)){  //pad
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
        }
        else{ //phone
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            switch (rotation) {
                case Surface.ROTATION_270:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {    //android 7.1以前，橫屏navigation bar恆在右邊；7.1以後則恆在靠近傳輸孔的位置
                        lp.setMargins(Navigation_bar_height, 0, 0, 0);
                        control_layout.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
                        lp.setMargins(Navigation_bar_height, Status_bar_height, 0, 0);
                        mToolbar.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                        lp.setMargins(0, 0, Navigation_bar_height, 0);
                        mToolbarTitle.setLayoutParams(lp);
                        break;
                    }   //android 7.1以前作法與90度一致，故不用break直接進到90的code
                case Surface.ROTATION_90:
                    lp.setMargins(0, 0, Navigation_bar_height, 0);
                    control_layout.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
                    lp.setMargins(0, Status_bar_height, Navigation_bar_height, 0);
                    mToolbar.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                    lp.setMargins(Navigation_bar_height, 0, 0, 0);
                    mToolbarTitle.setLayoutParams(lp);
                    break;
                case Surface.ROTATION_0:
                    break;
                case Surface.ROTATION_180:
                    break;
            }
        }
    }

    protected void adjustPortraitUi(){
        if (UiHelper.isPad(this)){  //pad
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
        }
        else{   //phone
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
            lp.setMargins(0, Status_bar_height,0, 0);
            mToolbar.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
            lp.setMargins(0, 0, 0, 0);
            mToolbarTitle.setLayoutParams(lp);
        }
    }

    private void adjust15sController(){ //調整快進快退15秒按鈕的位置
        UnitConverter converter = new UnitConverter(this);
        int width, groupMargin;
        width = mScreenW;

        if (delete.getVisibility() != View.GONE)
            width = width - (int) (delete.getWidth() * 2);
        if (download.getVisibility() != View.GONE)
            width = width - (int) (download.getWidth() * 2);

        groupMargin = (int) converter.convertPixelToDp(width/16);
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) prev.getLayoutParams();
        lp.setMargins(0,0, groupMargin,0);
        prev.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) next.getLayoutParams();
        lp.setMargins(groupMargin,0,0,0);
        next.setLayoutParams(lp);

        groupMargin = (int) converter.convertPixelToDp(width/24);
        lp = (ViewGroup.MarginLayoutParams) prev15.getLayoutParams();
        lp.setMargins(0,0, groupMargin,0);
        prev15.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) next15.getLayoutParams();
        lp.setMargins(groupMargin,0,0,0);
        next15.setLayoutParams(lp);
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        int difference = 0;
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                if(hasFocus){
                    System.out.println("onWindowFocusChanged");
                    difference = FrameworkConstant.mPortraitScreenHeight - control_layout.getBottom();
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustPortraitUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_90:
                if(hasFocus){   //由於需要判斷控件寬度，所以此處focus後要再調整一次
                    difference = FrameworkConstant.mPortraitScreenHeight - control_layout.getRight();
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustLandscapeUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_270:
                if(hasFocus){   //由於需要判斷控件寬度，所以此處focus後要再調整一次
                    difference = control_layout.getLeft() - Navigation_bar_height;
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustLandscapeUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_180:
                break;
        }
    }

    private SeekBar.OnSeekBarChangeListener OnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        private boolean isPlaying = false;

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            long totalDuration = vlc_player.getLength();
            int currentPosition = PlayerUtils.progressToTimer(mSeekbar.getProgress(), totalDuration);
            String finalTimerString = "";
            int hours = (int) (currentPosition / (1000*60*60));
            int minutes = (int) (currentPosition % (1000*60*60)) / (1000*60);
            int seconds = (int) ((currentPosition % (1000*60*60)) % (1000*60) / 1000);

            int hasHour = (int) (totalDuration / (1000*60*60));
            // Add hours if there
            if(hasHour > 0)
                finalTimerString = String.format("%d:%02d:%02d", hours, minutes, seconds);
            else
                finalTimerString = String.format("%02d:%02d", minutes, seconds);

            current_time.setText(finalTimerString);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            if (vlc_player.isPlaying())
                isPlaying = true;
            else
                isPlaying = false;
            pause();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            long totalDuration = vlc_player.getLength();
            int currentPosition = PlayerUtils.progressToTimer(seekBar.getProgress(), totalDuration);

            vlc_player.seekTo(currentPosition);

            if (isPlaying)
                play();
        }
    };

    protected void setPrevAndNextVisibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        prev.setVisibility(visibility);
        next.setVisibility(visibility);
    }

    protected void setPrev15AndNext15Visibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        prev15.setVisibility(visibility);
        next15.setVisibility(visibility);
    }

    protected void setDownloadAndDeleteVisibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        download.setVisibility(visibility);
        delete.setVisibility(visibility);
    }

    @Override
    public void onDestroy(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.release();
        super.onDestroy();
    }
}