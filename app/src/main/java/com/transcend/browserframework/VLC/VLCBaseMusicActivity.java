package com.transcend.browserframework.VLC;

import android.media.MediaMetadataRetriever;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.PlayerUtils;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

import org.videolan.libvlc.MediaPlayer;

import static com.transcend.browserframework.VLC.VLCPlayer.PlayerState_ReachEnd;

/**
 * Created by mike_chen on 2018/1/29.
 */

public class VLCBaseMusicActivity extends AppCompatActivity {
    private static String TAG = VLCBaseMusicActivity.class.getSimpleName();

    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private VLCPlayer vlc_player;
    private Handler mHandler = new Handler();

    private TextView album, artist; //專輯；演奏家
    private SeekBar mSeekbar;
    private TextView current_time, duration_time;    //現在時間；總時間
    private ImageView play, prev, next;
    private ImageView repeat, shuffle;  //重複播放；隨機播放
    private ImageView mMusicImage;
    private RelativeLayout progressView;    //Loading 圖示

    private OnMusicViewControllerListener musicControllerListener;
    public interface OnMusicViewControllerListener{
        void OnEndReached();
        void OnNextButtonPress();
        void OnPreviousButtonPress();
        void OnRepeatModeChange(RepeatMode mode);
        void OnShuffleModeChange(ShuffleMode mode);
    }
    public void setOnMusicViewControllerListener(OnMusicViewControllerListener listener){
        musicControllerListener = listener;
    }

    RepeatMode mRepeatMode = RepeatMode.Off;
    public enum RepeatMode{
        Off, All, One
    }

    ShuffleMode mShuffleMode = ShuffleMode.Off;
    public enum ShuffleMode{
        Off, Shuffle
    }

    protected void setMusicContentView() {
        //將UI的處理規劃另外作，再回傳給Activity
        LayoutInflater mInflater = LayoutInflater.from(this);
        View view = UiHelper.setMusicUiDimensioning(this, mInflater);
        setContentView(view);

        iniView();
        initPlayer();
        iniToolbar();

        mMusicImage.setImageResource(R.drawable.img_singleview_music);

        //System bar 透明化
        UiHelper.setSystemBarTranslucent(this);
    }

    private void iniView(){
        album = (TextView) findViewById(R.id.music_album);
        artist = (TextView) findViewById(R.id.music_artist);

        mSeekbar = (SeekBar) findViewById(R.id.music_seekbar);
        mSeekbar.setOnSeekBarChangeListener(OnSeekBarChangeListener);

        current_time = (TextView) findViewById(R.id.music_current_time);
        duration_time = (TextView) findViewById(R.id.music_duration);

        play = (ImageView) findViewById(R.id.music_play);
        prev = (ImageView) findViewById(R.id.music_previous);
        next = (ImageView) findViewById(R.id.music_next);

        repeat = (ImageView) findViewById(R.id.music_repeat);
        shuffle = (ImageView) findViewById(R.id.music_shuffle);

        mMusicImage = (ImageView) findViewById(R.id.music_image);
        progressView = (RelativeLayout) findViewById(R.id.music_progress_view);

        if (UiHelper.isPad(this))
            mMusicImage.setScaleType(ImageView.ScaleType.CENTER);
        else
            mMusicImage.setScaleType(ImageView.ScaleType.FIT_CENTER);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vlc_player.isPlaying())
                    pause();
                else
                    play();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vlc_player.isPlaying())
                    stop();
                musicControllerListener.OnNextButtonPress();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (vlc_player.isPlaying())
                    stop();
                musicControllerListener.OnPreviousButtonPress();
            }
        });

        shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mShuffleMode){
                    case Off:
                        mShuffleMode = ShuffleMode.Shuffle;
                        setShuffleImageResource(R.drawable.ic_shuffle_on);
                        break;
                    default:
                        mShuffleMode = ShuffleMode.Off;
                        setShuffleImageResource(R.drawable.ic_shuffle_off);
                        break;
                }
                musicControllerListener.OnShuffleModeChange(mShuffleMode);
            }
        });

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (mRepeatMode){
                    case Off:
                        mRepeatMode = RepeatMode.All;
                        setRepeatImageResource(R.drawable.ic_repeat_all);
                        break;
                    case All:
                        mRepeatMode = RepeatMode.One;
                        setRepeatImageResource(R.drawable.ic_repeat_one);
                        break;
                    default:
                        mRepeatMode = RepeatMode.Off;
                        setRepeatImageResource(R.drawable.ic_repeat_off);
                        break;
                }
                musicControllerListener.OnRepeatModeChange(mRepeatMode);
            }
        });
    }

    private void initPlayer(){
        vlc_player = new VLCPlayer(this);
    }

    protected void setupMusic(String url, boolean isRemote){
        mSeekbar.setProgress(0);
        duration_time.setText("00:00");
        current_time.setText("00:00");

        loadMusicInformation(url);
        vlc_player.setDataSource(url, isRemote);
        vlc_player.setDisplay(null, null);
    }

    protected void startUpdatingProgressBar() {
        mHandler.postDelayed(mUpdateTimeTask, 500);
    }

    private void iniToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.music_header_bar);
        mToolbarTitle = (TextView) findViewById(R.id.music_toolbar_title);
        UnitConverter converter = new UnitConverter(this);
        mToolbarTitle.setTextSize(converter.convertPtToSp(34));
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    protected void loadMusicInformation(String url){
        MediaMetadataRetriever mmr = new MediaMetadataRetriever();
        try {
            mmr.setDataSource(url);
            String title = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_TITLE); // api level 10, 即从GB2.3.3开始有此功能
            String album = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ALBUM);
            String mime = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_MIMETYPE);
            String artist = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_ARTIST);
            String duration = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION); // 播放时长单位为毫秒
            String bitrate = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_BITRATE); // 从api level 14才有，即从ICS4.0才有此功能
            String date = mmr.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DATE);

//            Log.e(TAG, "title:" + title);
//            Log.e(TAG, "album:" + album);
//            Log.e(TAG, "mime:" + mime);
//            Log.e(TAG, "artist:" + artist);
//            Log.e(TAG, "duration:" + duration);
//            Log.e(TAG, "bitrate:" +bitrate);
//            Log.e(TAG, "date:" + date);
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        } catch (IllegalStateException e) {
            e.printStackTrace();
        }
    }

    protected void setShuffleImageResource(int resId){
        shuffle.setImageResource(resId);
    }

    protected void setRepeatImageResource(int resId){
        repeat.setImageResource(resId);
    }

    protected ImageView getMusicAlbumImageView(){
        return mMusicImage;
    }

    protected void setAlbumText(String text){
        album.setText(text);
    }

    protected void setArtistText(String text){
        artist.setText(text);
    }

    protected Toolbar getToolbar(){
        return mToolbar;
    }

    protected void setToolbarTitle(String text){
        mToolbarTitle.setText(text);
    }

    protected boolean isPlaying(){
        return vlc_player.isPlaying();
    }

    protected void startPlaying(){
        play();
    }

    protected void stopPlaying(){
        stop();
    }

    protected void enablePrevBtn(boolean enable){
        prev.setEnabled(enable);
        if (enable)
            prev.setImageResource(R.drawable.ic_prev_large);
        else
            prev.setImageResource(R.drawable.ic_singleview_playback_grey);
    }

    protected void enableNextBtn(boolean enable){
        next.setEnabled(enable);
        if (enable)
            next.setImageResource(R.drawable.ic_next_large);
        else
            next.setImageResource(R.drawable.ic_singleview_playnext_grey);
    }

    private SeekBar.OnSeekBarChangeListener OnSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        private boolean isPlaying = false;  //紀錄之前的狀態是否為播放中

        @Override
        public void onProgressChanged(SeekBar seekBar, int progress, boolean fromTouch) {
            long totalDuration = vlc_player.getLength();
            int currentPosition = PlayerUtils.progressToTimer(mSeekbar.getProgress(), totalDuration);
            String finalTimerString = "";
            int hours = (int) (currentPosition / (1000*60*60));
            int minutes = (int) (currentPosition % (1000*60*60)) / (1000*60);
            int seconds = (int) ((currentPosition % (1000*60*60)) % (1000*60) / 1000);

            int hasHour = (int) (totalDuration / (1000*60*60));
            // Add hours if there
            if(hasHour > 0)
                finalTimerString = String.format("%d:%02d:%02d", hours, minutes, seconds);
            else
                finalTimerString = String.format("%02d:%02d", minutes, seconds);

            current_time.setText(finalTimerString);
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {
            if (vlc_player.isPlaying())
                isPlaying = true;
            else
                isPlaying = false;
            pause();
        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {
            long totalDuration = vlc_player.getLength();
            int currentPosition = PlayerUtils.progressToTimer(seekBar.getProgress(), totalDuration);

            vlc_player.seekTo(currentPosition);

            if (isPlaying)
                play();
        }
    };

    private Runnable mUpdateTimeTask = new Runnable() {
        public void run() {
            long totalDuration = vlc_player.getLength();
            long currentDuration = vlc_player.getTime();

            if (mSeekbar.getProgress() == 100){
                stop();
                mMusicImage.setVisibility(View.VISIBLE);

                musicControllerListener.OnEndReached();
                return;
            }

            if (vlc_player.getPlayerState() == PlayerState_ReachEnd){   //目前VLC不會播到最後，會在前一刻N毫秒前停止
                updateTimeText(totalDuration, totalDuration);
                mSeekbar.setProgress(200);
            }
            else {
                // Updating progress bar
                int progress = (int) (PlayerUtils.getProgressPercentage(currentDuration, totalDuration));
                mSeekbar.setProgress(progress);
                updateTimeText(currentDuration, totalDuration);
            }

            // Running this thread after 200 milliseconds
            mHandler.postDelayed(this, 200);
        }
    };

    private void updateTimeText(long current, long duration){
        String durationText = PlayerUtils.milliSecondsToTimer(duration);
        // Displaying Total Duration time
        duration_time.setText(durationText);

        // Displaying time completed playing
        if (durationText.length() == "00:00:00".length())
            current_time.setText(PlayerUtils.milliSecondsToTimer(current, true));
        else
            current_time.setText(PlayerUtils.milliSecondsToTimer(current, false));
    }

    protected void play(){
        vlc_player.start();
        startUpdatingProgressBar();
        play.setImageResource(R.drawable.ic_pause);
    }

    protected void pause(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.pause();
        play.setImageResource(R.drawable.ic_play);
    }

    protected void stop(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.stop();
        play.setImageResource(R.drawable.ic_play);
        mSeekbar.setProgress(0);
    }

    protected void setLoadingProgressVisibility(int visible){
        if (visible == View.VISIBLE || visible == View.GONE || visible == View.INVISIBLE)
            progressView.setVisibility(visible);
    }

    protected void setShuffleButtonVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE){
            shuffle.setVisibility(visibility);
        }
    }

    protected void setRepeatButtonVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE){
            repeat.setVisibility(visibility);
        }
    }

    protected void setAlbumTextVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            album.setVisibility(visibility);
    }

    protected void setArtistTextVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            artist.setVisibility(visibility);
    }

    //設定這邊要與參數一致
    protected void setShuffleMode(ShuffleMode mode){
        switch (mode){
            case Off:
                mShuffleMode = ShuffleMode.Off;
                setShuffleImageResource(R.drawable.ic_shuffle_off);
                break;
            default:
                mShuffleMode = ShuffleMode.Shuffle;
                setShuffleImageResource(R.drawable.ic_shuffle_on);
                break;
        }
    }

    //設定這邊要與參數一致
    protected void setRepeatMode(RepeatMode mode){
        switch (mRepeatMode){
            case Off:
                mRepeatMode = RepeatMode.Off;
                setRepeatImageResource(R.drawable.ic_repeat_off);
                break;
            case All:
                mRepeatMode = RepeatMode.All;
                setRepeatImageResource(R.drawable.ic_repeat_all);
                break;
            default:
                mRepeatMode = RepeatMode.One;
                setRepeatImageResource(R.drawable.ic_repeat_one);
                break;
        }
    }

    @Override
    public void onDestroy(){
        mHandler.removeCallbacks(mUpdateTimeTask);
        vlc_player.release();
        super.onDestroy();
    }
}