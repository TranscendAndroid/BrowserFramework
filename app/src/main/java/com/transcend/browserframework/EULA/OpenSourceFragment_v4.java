package com.transcend.browserframework.EULA;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.transcend.browserframework.R;

import java.util.Stack;

public class OpenSourceFragment_v4 extends Fragment {
    private WebView mWebView;
    private RelativeLayout mProgress;
    private Stack<String> mUrls;

    private static String ops_url = "";

    public static OpenSourceFragment_v4 newInstance(String URL){
        OpenSourceFragment_v4 f = new OpenSourceFragment_v4();
        ops_url = URL;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.fragment_webview, container, false);
        mProgress = (RelativeLayout) root.findViewById(R.id.progress_view);
        mProgress.setVisibility(View.VISIBLE);
        mWebView = (WebView) root.findViewById(R.id.webview);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);

        mWebView.setPadding(10, 20,  10, 0);

        String url = ops_url;
        mUrls = new Stack<>();
        mUrls.push(url);
        mWebView.loadUrl(url);

        return root;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if(mWebView.getProgress() == 100) {
                mProgress.setVisibility(View.INVISIBLE);
                if (mUrls != null) {
                    if(url == null)
                        return;

                    String origin = ops_url;
                    if(origin.equals(url)) {
                        mUrls.clear();
                        mUrls.push(url);
                        return;
                    }

                    if(!mUrls.isEmpty() && !url.equals(mUrls.peek())) {
                        mUrls.push(url);
                        return;
                    }
                }
            }
        }
    }

    //判斷是否回到最初的頁面
    public boolean isOriginalPage() {
        if(mUrls != null && !mUrls.isEmpty()) {
            String origin = ops_url;
            if (!origin.equals(mUrls.peek())) {
                mUrls.pop();
                if (!mUrls.isEmpty()) {
                    mWebView.loadUrl(mUrls.peek());
                    return false;
                }
            }
        }
        return true;
    }
}
