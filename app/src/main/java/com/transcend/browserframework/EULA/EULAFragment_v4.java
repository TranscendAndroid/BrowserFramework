package com.transcend.browserframework.EULA;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.transcend.browserframework.R;

import java.util.Stack;

public class EULAFragment_v4 extends Fragment {

    private Context mContext;
    static boolean showAgreeBtn = true;
    private WebView mWebView;
    private RelativeLayout mProgress;
    private Stack<String> mUrls;

    private String eula_url = "file:///android_asset/EULA.html";

    private OnEULAClickListener mListener;
    public interface OnEULAClickListener{
        void OnAgreeBtnClick();
    }
    public void setOnEULAClickListener(OnEULAClickListener listener){
        mListener = listener;
    }

    private Button agreeBtn;
    private boolean hasEnabled = false;

    public static EULAFragment_v4 newInstance(boolean showAgreeButton){
        EULAFragment_v4 f = new EULAFragment_v4();
        showAgreeBtn = showAgreeButton;
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private boolean checkContext(){
        if (mContext == null)
            mContext = getActivity();

        return mContext != null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.fragment_eula_agreement, container, false);
        mProgress = (RelativeLayout) root.findViewById(R.id.progress_view);
        mProgress.setVisibility(View.VISIBLE);
        mWebView = (WebView) root.findViewById(R.id.eula_webview);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mWebView.getLayoutParams();
        lp.setMargins(10, 20,  10, 0);
        mWebView.setLayoutParams(lp);

        String url = eula_url;
        mUrls = new Stack<>();
        mUrls.push(url);
        mWebView.loadUrl(url);


        agreeBtn = (Button) root.findViewById(R.id.agree_btn);
        agreeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mListener == null) {
                    if (checkContext())
                        Toast.makeText(mContext, "Not set listener yet!!", Toast.LENGTH_SHORT).show();
                } else
                    mListener.OnAgreeBtnClick();
            }
        });
        agreeBtn.setEnabled(false);
        if (!showAgreeBtn)
            agreeBtn.setVisibility(View.GONE);
        else
            agreeBtn.setVisibility(View.VISIBLE);

        mWebView.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {
            @Override
            public void onScrollChanged() {
                int height = (int) Math.floor(mWebView.getContentHeight() * mWebView.getScale());
                int webViewHeight = mWebView.getMeasuredHeight();
                if(mWebView.getScrollY() + webViewHeight >= (height-80)){   //誤差80
                    if (isOriginalPageWithoutPop()) {
                        //scroll view is at bottom
                        hasEnabled = true;
                        enableAgreeBtn(true);
                    }
                }
            }
        });

        return root;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if(mWebView.getProgress() == 100) {
                mProgress.setVisibility(View.INVISIBLE);
                if (mUrls != null) {
                    if(url == null)
                        return;

                    String origin = eula_url;
                    if(origin.equals(url)) {
                        mUrls.clear();
                        mUrls.push(url);
                        if (showAgreeBtn) {
                            agreeBtn.setVisibility(View.VISIBLE);
                            if (hasEnabled)
                                enableAgreeBtn(true);
                            else
                                enableAgreeBtn(false);
                        }
                        return;
                    }
                    else {
                        agreeBtn.setVisibility(View.GONE);
                    }

                    if(!mUrls.isEmpty() && !url.equals(mUrls.peek())) {
                        mUrls.push(url);
                        return;
                    }
                }
            }
        }
    }

    public void enableAgreeBtn(boolean enable){
        agreeBtn.setEnabled(enable);
        if (checkContext())
            agreeBtn.setTextColor(mContext.getResources().getColor(R.color.c_06));
        else
            agreeBtn.setTextColor(mContext.getResources().getColor(R.color.c_04));
    }

    public boolean isOriginalPageWithoutPop() {
        if(mUrls != null && !mUrls.isEmpty()) {
            String origin = eula_url;
            return origin.equals(mUrls.peek());
        }
        return true;
    }

    //判斷是否回到最初的頁面
    public boolean isOriginalPage() {
        if(mUrls != null && !mUrls.isEmpty()) {
            String origin = eula_url;
            if (!origin.equals(mUrls.peek())) {
                mUrls.pop();
                if (!mUrls.isEmpty()) {
                    mWebView.loadUrl(mUrls.peek());
                    return false;
                }
            }
        }
        return true;
    }
}