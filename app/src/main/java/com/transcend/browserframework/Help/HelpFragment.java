package com.transcend.browserframework.Help;

import android.app.Fragment;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.RelativeLayout;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.PagerSwipeRefreshLayout;

import java.util.Stack;

public class HelpFragment extends Fragment {
    private PagerSwipeRefreshLayout mSwipeRefreshLayout;
    private WebView mWebView;
    private RelativeLayout mProgress;
    private Stack<String> mUrls;

    static String help_url = "https://us.transcend-info.com/support/service";

    public static HelpFragment newInstance(String url){
        HelpFragment f = new HelpFragment();
        help_url = url;
        return f;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.fragment_webview, container, false);

        mProgress = (RelativeLayout) root.findViewById(R.id.progress_view);
        mProgress.setVisibility(View.VISIBLE);
        mWebView = (WebView) root.findViewById(R.id.webview);
        mWebView.setWebViewClient(new MyWebViewClient());
        mWebView.getSettings().setJavaScriptEnabled(true);

        mSwipeRefreshLayout = root.findViewById(R.id.swipe_layout);
        mSwipeRefreshLayout.setColorSchemeResources(R.color.c_06);
        mSwipeRefreshLayout.setOnRefreshListener(new PagerSwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mWebView.reload();
            }
        });

        String url = help_url;
        mUrls = new Stack<>();
        mUrls.push(url);
        mWebView.loadUrl(url);

        return root;
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            mSwipeRefreshLayout.setRefreshing(false);
            mProgress.setVisibility(View.VISIBLE);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            mProgress.setVisibility(View.INVISIBLE);
            if (mUrls != null) {
                if(url == null)
                    return;

                String origin = help_url;
                if(origin.equals(url)) {
                    mUrls.clear();
                    mUrls.push(url);
                    return;
                }

                if(!mUrls.isEmpty() && !url.equals(mUrls.peek())) {
                    mUrls.push(url);
                    return;
                }
            }
        }
    }

    //判斷是否回到最初的頁面
    public boolean isOriginalPage() {
        if(mUrls != null && !mUrls.isEmpty()) {
            String origin = help_url;
            if (!origin.equals(mUrls.peek())) {
                mUrls.pop();
                if (!mUrls.isEmpty()) {
                    mWebView.loadUrl(mUrls.peek());
                    return false;
                }
            }
        }
        return true;
    }
}
