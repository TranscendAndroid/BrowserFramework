package com.transcend.browserframework.SingleView;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.os.Build;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

/**
 * Created by mike_chen on 2018/1/30.
 */

public class ImageBaseActivity extends AppCompatActivity{

    private Toolbar mToolbar;
    private TextView mToolbarTitle;
    protected int mScreenW = 0, mScreenH = 0;   //螢幕長寬，此處的Height有包含Navigation bar
    private ViewPager mPager; //用於左右滑切換圖片
    private RelativeLayout progressView;

    private static int Navigation_bar_height = 0;
    private static int Status_bar_height = 0;
    private static int Notch_height = 0;

    //監控螢幕方向變化
    private WindowManager mWindowManager = null;
    private int mLastRotation = -1;

    LinearLayout footer;

    //TODO onCreate來宣告即可，待新增

    protected void setImageContentView() {
        //將UI的處理規劃另外作，再回傳給Activity
        setContentView(R.layout.single_view_image);

        //避免沒更新到螢幕大小
        if(FrameworkConstant.mPortraitScreenWidth == 0 || FrameworkConstant.mPortraitScreenHeight == 0)
            UiHelper.updateScreenSize(this);

        Navigation_bar_height = UiHelper.getNavigationBarHeight(this);
        Status_bar_height = UiHelper.getStatusBarHeight(this);
        Notch_height = UiHelper.getNotchHeight(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            mScreenW = FrameworkConstant.mPortraitScreenWidth;
            mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
        } else {
            mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
            mScreenH = FrameworkConstant.mPortraitScreenWidth;
        }
        mPager = (ViewPager) findViewById(R.id.photo_view_pager);

        iniToolbar();
        setOrientationListener();

        progressView = (RelativeLayout) findViewById(R.id.viewer_progress_view);

        //System bar 透明化
        UiHelper.setSystemBarTranslucent(this);

        if (this.getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE)
            adjustLandscapeUi();
        else
            adjustPortraitUi();
    }

    private void iniToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        mToolbarTitle = (TextView) findViewById(R.id.toolbar_title);

        UnitConverter converter = new UnitConverter(this);
        mToolbarTitle.setTextSize(converter.convertPtToSp(34));
        mToolbar.setNavigationIcon(R.drawable.ic_back);

        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //瀏覽圖片時把toolbar弄半透明
        mToolbar.setBackgroundColor( getResources().getColor(R.color.colorBlack_transparency));
        //全螢幕後避免actionbar被statusbar擋住
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
        lp.setMargins(0, Status_bar_height, 0, 0);
        mToolbar.setLayoutParams(lp);

        //預設隱藏，有物件加入才顯示
        footer = (LinearLayout) findViewById(R.id.viewer_footer_bar);
        footer.setVisibility(View.GONE);
    }

    protected Toolbar getToolbar(){
        return mToolbar;
    }

    protected void setToolbarTitle(String text){
        mToolbarTitle.setText(text);
    }

    protected ViewPager getViewPager(){
        return mPager;
    }

    protected void setLoadingProgressVisibility(int visible){
        if (visible == View.VISIBLE || visible == View.GONE || visible == View.INVISIBLE)
            progressView.setVisibility(visible);
    }

    private void setOrientationListener(){  //以角度判斷橫屏、直屏
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        OrientationEventListener orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {

                Display display = mWindowManager.getDefaultDisplay();
                int rotation = display.getRotation();
                if ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && rotation != mLastRotation){
                    Status_bar_height = UiHelper.getStatusBarHeight(ImageBaseActivity.this);
                    Notch_height = UiHelper.getNotchHeight(ImageBaseActivity.this);

                    adjustLandscapeUi();
                    mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
                    mScreenH = FrameworkConstant.mPortraitScreenWidth;

                    mLastRotation = rotation;
                } else if (rotation == Surface.ROTATION_0 && rotation != mLastRotation){
                    Status_bar_height = UiHelper.getStatusBarHeight(ImageBaseActivity.this);
                    Notch_height = UiHelper.getNotchHeight(ImageBaseActivity.this);

                    adjustPortraitUi();
                    mScreenW = FrameworkConstant.mPortraitScreenWidth;
                    mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。

                    mLastRotation = rotation;
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        }
    }

    protected void adjustLandscapeUi(){
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();

        if (UiHelper.isPad(this)){
            lp.setMargins(0, Status_bar_height,0, 0);
            mToolbar.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) footer.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            footer.setLayoutParams(lp);
        } else {
            switch (rotation) {
                case Surface.ROTATION_270:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {    //android 7.1以前，橫屏navigation bar恆在右邊；7.1以後則恆在靠近傳輸孔的位置
                        lp.setMargins(Navigation_bar_height, Status_bar_height, 0, 0);
                        mToolbar.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                        lp.setMargins(0, 0, Navigation_bar_height, 0);
                        mToolbarTitle.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) footer.getLayoutParams();
                        lp.setMargins(Navigation_bar_height, 0, 0, 0);
                        footer.setLayoutParams(lp);
                        break;
                    }   //android 7.1以前作法與90度一致，故不用break直接進到90的code
                case Surface.ROTATION_90:
                    lp.setMargins(0, Status_bar_height, Navigation_bar_height, 0);
                    mToolbar.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                    lp.setMargins(Navigation_bar_height, 0, 0, 0);
                    mToolbarTitle.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) footer.getLayoutParams();
                    lp.setMargins(0, 0, Navigation_bar_height, 0);
                    footer.setLayoutParams(lp);
                    break;
                case Surface.ROTATION_0:
                    break;
                case Surface.ROTATION_180:
                    break;
            }
        }
    }

    protected void adjustPortraitUi(){
        if (UiHelper.isPad(this)){  //pad
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) footer.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            footer.setLayoutParams(lp);
        } else {   //phone
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
            lp.setMargins(0, Status_bar_height, 0, 0);
            mToolbar.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
            lp.setMargins(0, 0, 0, 0);
            mToolbarTitle.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) footer.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            footer.setLayoutParams(lp);
        }
    }

    protected int getScreenHeight(){
        return mScreenH;
    }

    protected int getScreenWidth(){
        return mScreenW;
    }

    protected void setToolbarTitleIsSingleLine(boolean enable){ //設置ToolbarTitle是否為singleLine(For DPB)
        mToolbarTitle.setSingleLine(enable);
    }

    protected void addFooterItem(View view){
        footer.setVisibility(View.VISIBLE);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT);
        params.weight = 1;

        footer.addView(view, params);
    }

    protected void setFooterVisibility(int visibility){
        if (visibility == View.VISIBLE || visibility == View.INVISIBLE || visibility == View.GONE)
            footer.setVisibility(visibility);
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);

        if (hasFocus)
            checkNavigationBar();
    }

    private void checkNavigationBar(){
        int difference = 0;

        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                //判斷footer底部需與整個高一致，否則必為過度位移
                difference = FrameworkConstant.mPortraitScreenHeight - footer.getBottom();
                if (difference > Navigation_bar_height / 2){
                    Navigation_bar_height = 0;
                    adjustPortraitUi();
                }
                break;
            case Surface.ROTATION_90:
                difference = FrameworkConstant.mPortraitScreenHeight - footer.getRight();
                if (difference > Navigation_bar_height / 2){
                    Navigation_bar_height = 0;
                    adjustLandscapeUi();
                }
                break;
            case Surface.ROTATION_180:
                break;
            case Surface.ROTATION_270:
                difference = footer.getLeft() - Navigation_bar_height;
                if (difference > Navigation_bar_height / 2){
                    Navigation_bar_height = 0;
                    adjustLandscapeUi();
                }
                break;
        }
    }
}