package com.transcend.browserframework.SingleView;

import android.content.Context;
import android.content.res.Configuration;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.os.Handler;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.OrientationEventListener;
import android.view.Surface;
import android.view.SurfaceView;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.FrameworkConstant;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

/**
 * Created by mike_chen on 2018/1/31.
 */

public class VideoBaseActivity extends AppCompatActivity{

    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private RelativeLayout control_layout;  //上一首、播放、下一首的layout，方便全螢幕時隱藏
    private ImageView play, prev, next, prev15, next15, delete, download;

    private SeekBar mSeekbar;

    private TextView current_time, duration;    //現在時間；總時間

    private RelativeLayout image_layout;
    private ImageView video_image;  //影片預覽圖

    private RelativeLayout progressView;    //Loading 圖示

    //監控螢幕方向變化
    private WindowManager mWindowManager = null;
    private int mLastRotation = -1;

    private Handler handler = new Handler();
    protected boolean curControllerHide;

    private SurfaceView surfaceView;
    private MediaPlayer mediaPlayer;

    protected int mScreenW, mScreenH;
    protected boolean hideControlLayout = false;

    private ProgressBar mLoadingProgress;

    private static int Navigation_bar_height = 0;
    private static int Status_bar_height = 0;
    private static int Notch_height = 0;

    //TODO 改為abstract
    private OnVideoViewControllerListener videoViewControllerListener;
    public interface OnVideoViewControllerListener{
        void playButtonPress();
        void nextButtonPress();
        void previousButtonPress();
        void next15ButtonPress();
        void previous15ButtonPress();
        void downloadButtonPress();
        void deleteButtonPress();
    }
    public void setOnVideoViewControllerListener(OnVideoViewControllerListener listener){
        videoViewControllerListener = listener;
    }

    protected void setVideoContentView() {
        //將UI的處理規劃另外作，再回傳給Activity
        LayoutInflater mInflater = LayoutInflater.from(this);
        View view = UiHelper.setVideoUiDimensioning(this, mInflater);
        setContentView(view);

        //避免沒更新到螢幕大小
        if(FrameworkConstant.mPortraitScreenWidth == 0 || FrameworkConstant.mPortraitScreenHeight == 0)
            UiHelper.updateScreenSize(this);

        Navigation_bar_height = UiHelper.getNavigationBarHeight(this);
        Status_bar_height = UiHelper.getStatusBarHeight(this);
        Notch_height = UiHelper.getNotchHeight(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT){
            mScreenW = FrameworkConstant.mPortraitScreenWidth;
            mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
        }
        else{
            mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
            mScreenH = FrameworkConstant.mPortraitScreenWidth;
        }

        iniView();
        iniToolbar();
        setOrientationListener();

        progressView = (RelativeLayout) findViewById(R.id.video_progress_view);
        mLoadingProgress = findViewById(R.id.loading_progress);
        mLoadingProgress.setProgressDrawable(getResources().getDrawable(R.drawable.progressbar_style));

        //System bar 透明化
        //UiHelper.setSystemBarTranslucent(this);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setNavigationBarColor(ContextCompat.getColor(this, R.color.colorBlack));
            getWindow().setStatusBarColor(ContextCompat.getColor(this, R.color.colorBlack));
        }
        UiHelper.showBothSystemBarWithoutTranslucent(this);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE){
            adjustLandscapeUi();
        } else{
            adjustPortraitUi();
        }
    }

    protected void setPrevAndNextVisibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        prev.setVisibility(visibility);
        next.setVisibility(visibility);
    }

    protected void setPrev15AndNext15Visibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        prev15.setVisibility(visibility);
        next15.setVisibility(visibility);
    }

    protected void setDownloadAndDeleteVisibility(int visibility){
        if (visibility != View.VISIBLE && visibility != View.INVISIBLE && visibility != View.GONE)
            return;
        download.setVisibility(visibility);
        delete.setVisibility(visibility);
    }

    private void iniView(){

        control_layout = (RelativeLayout) findViewById(R.id.control_layout);
        play = (ImageView) findViewById(R.id.video_play);
        prev = (ImageView) findViewById(R.id.video_previous);
        next = (ImageView) findViewById(R.id.video_next);
        prev15 = (ImageView) findViewById(R.id.video_previous15);
        next15 = (ImageView) findViewById(R.id.video_next15);
        delete = (ImageView) findViewById(R.id.video_delete);
        download = (ImageView) findViewById(R.id.video_download);

        mSeekbar = (SeekBar) findViewById(R.id.video_seekbar);

        current_time = (TextView) findViewById(R.id.video_current_time);
        duration = (TextView) findViewById(R.id.video_duration);

        video_image = (ImageView) findViewById(R.id.video_image);
        image_layout = (RelativeLayout) findViewById(R.id.image_layout);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.playButtonPress();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.nextButtonPress();
            }
        });

        prev15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.previous15ButtonPress();
            }
        });

        next15.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.next15ButtonPress();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.previousButtonPress();
            }
        });

        delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.deleteButtonPress();
            }
        });

        download.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                videoViewControllerListener.downloadButtonPress();
            }
        });

        surfaceView = (SurfaceView) findViewById(R.id.surface_view);
        surfaceView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideControlLayout = !hideControlLayout;
                hideControllers(hideControlLayout);
            }
        });

        mediaPlayer = new MediaPlayer();
        mediaPlayer.setOnVideoSizeChangedListener(new MediaPlayer.OnVideoSizeChangedListener() {
            @Override
            public void onVideoSizeChanged(MediaPlayer mp, int width, int height) {
                changeVideoSize();
            }
        });
    }

    private void iniToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.video_header_bar);
        mToolbarTitle = (TextView) findViewById(R.id.video_toolbar_title);
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        UnitConverter converter = new UnitConverter(this);
        mToolbarTitle.setTextSize(converter.convertPtToSp(34));
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        //全螢幕後避免actionbar被statusbar擋住
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
        lp.setMargins(0, Status_bar_height, 0, 0);
        mToolbar.setLayoutParams(lp);
    }

    protected void hideControllers(boolean hide){
        if(hide){
            control_layout.setVisibility(View.GONE);
            mToolbar.setVisibility(View.GONE);

            //隱藏System bar (Navigation bar, status bar)
            UiHelper.hideBothSystemBarWithoutTranslucent(this);
            curControllerHide = true;
        }
        else{
            control_layout.setVisibility(View.VISIBLE);
            mToolbar.setVisibility(View.VISIBLE);

            //隱藏Navigation bar (Back, Home, Apps)
            UiHelper.showBothSystemBarWithoutTranslucent(this);
            curControllerHide = false;
        }
    }

    protected void setAutoHideControllerUI(int millisecond){
        handler.postDelayed(prepareToHideControllerUI, millisecond);
    }

    protected void cancelAutoHideControllerUI(){
        handler.removeCallbacks(prepareToHideControllerUI);
    }

    protected Runnable prepareToHideControllerUI = new Runnable() {
        @Override
        public void run() {
            hideControllers(true);
        }
    };

    protected void setPlayBtnImageResource(int resId){
        play.setImageResource(resId);
    }

    protected void setCurrentTimeText(String text){
        current_time.setText(text);
    }

    protected void setDurationText(String text){
        duration.setText(text);
    }

    protected ImageView getVideoImageView(){
        return video_image;
    }

    protected SeekBar getSeekbar(){
        return mSeekbar;
    }

    protected Toolbar getToolbar(){
        return mToolbar;
    }

    protected void setToolbarTitle(String text){
        mToolbarTitle.setText(text);
    }

    protected int getScreenHeight(){
        return mScreenH;
    }

    protected int getScreenWidth(){
        return mScreenW;
    }

    //設置ToolbarTitle是否為singleLine(For DPB)
    protected void setToolbarTitleIsSingleLine(boolean enable){
        mToolbarTitle.setSingleLine(enable);
    }

    protected SurfaceView getSurfaceView(){
        return surfaceView;
    }

    protected MediaPlayer getMediaPlayer(){
        return mediaPlayer;
    }

    protected void enablePrevBtn(boolean enable){
        prev.setEnabled(enable);
        if (enable)
            prev.setImageResource(R.drawable.ic_prev);
        else
            prev.setImageResource(R.drawable.ic_singleview_playback_s_grey);
    }

    protected void enableNextBtn(boolean enable){
        next.setEnabled(enable);
        if (enable)
            next.setImageResource(R.drawable.ic_next);
        else
            next.setImageResource(R.drawable.ic_singleview_playnext_s_grey);
    }

    protected void setLoadingProgressVisibility(int visible){
        if (visible == View.VISIBLE || visible == View.GONE || visible == View.INVISIBLE)
            progressView.setVisibility(visible);
    }

    protected void setOrientationListener(){
        mWindowManager = (WindowManager) getSystemService(Context.WINDOW_SERVICE);
        OrientationEventListener orientationEventListener = new OrientationEventListener(this,
                SensorManager.SENSOR_DELAY_NORMAL) {
            @Override
            public void onOrientationChanged(int orientation) {

                Display display = mWindowManager.getDefaultDisplay();
                int rotation = display.getRotation();
                if ((rotation == Surface.ROTATION_90 || rotation == Surface.ROTATION_270) && rotation != mLastRotation) {
                    Status_bar_height = UiHelper.getStatusBarHeight(VideoBaseActivity.this);
                    Notch_height = UiHelper.getNotchHeight(VideoBaseActivity.this);

                    adjustLandscapeUi();

                    mScreenW = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
                    mScreenH = FrameworkConstant.mPortraitScreenWidth;
                    mLastRotation = rotation;
                }
                else if (rotation == Surface.ROTATION_0 && mLastRotation != 0){
                    Status_bar_height = UiHelper.getStatusBarHeight(VideoBaseActivity.this);
                    Notch_height = UiHelper.getNotchHeight(VideoBaseActivity.this);

                    adjustPortraitUi();

                    mScreenW = FrameworkConstant.mPortraitScreenWidth;
                    mScreenH = FrameworkConstant.mPortraitScreenHeight + Navigation_bar_height; //原本的Height是沒有計算Navigation bar的高度，為了全屏顯示此處加回來。
                    mLastRotation = rotation;
                }
            }
        };

        if (orientationEventListener.canDetectOrientation()) {
            orientationEventListener.enable();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
//        int orientation=newConfig.orientation;
//        switch(orientation) {
//            case Configuration.ORIENTATION_LANDSCAPE:
//                adjustLandscapeUi();
//                break;
//            case Configuration.ORIENTATION_PORTRAIT:
//                adjustPortraitUi();
//                break;
//        }
        changeVideoSize();
        super.onConfigurationChanged(newConfig);
    }

    protected void adjustLandscapeUi(){
        if (UiHelper.isPad(this)){  //pad
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
        }
        else{ //phone
            int rotation = getWindowManager().getDefaultDisplay().getRotation();
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            switch (rotation) {
                case Surface.ROTATION_270:
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {    //android 7.1以前，橫屏navigation bar恆在右邊；7.1以後則恆在靠近傳輸孔的位置
                        lp.setMargins(Navigation_bar_height, 0, 0, 0);
                        control_layout.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
                        lp.setMargins(Navigation_bar_height, Status_bar_height, 0, 0);
                        mToolbar.setLayoutParams(lp);
                        lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                        lp.setMargins(0, 0, Navigation_bar_height, 0);
                        mToolbarTitle.setLayoutParams(lp);
                        break;
                    }   //android 7.1以前作法與90度一致，故不用break直接進到90的code
                case Surface.ROTATION_90:
                    lp.setMargins(0, 0, Navigation_bar_height, 0);
                    control_layout.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
                    lp.setMargins(0, Status_bar_height, Navigation_bar_height, 0);
                    mToolbar.setLayoutParams(lp);
                    lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
                    lp.setMargins(Navigation_bar_height, 0, 0, 0);
                    mToolbarTitle.setLayoutParams(lp);
                    break;
                case Surface.ROTATION_0:
                    break;
                case Surface.ROTATION_180:
                    break;
            }
        }
        adjust15sController();
    }

    protected void adjustPortraitUi(){
        if (UiHelper.isPad(this)){  //pad
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
        }
        else{   //phone
            ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) control_layout.getLayoutParams();
            lp.setMargins(0, 0, 0, Navigation_bar_height);
            control_layout.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) mToolbar.getLayoutParams();
            lp.setMargins(0, Status_bar_height,0, 0);
            mToolbar.setLayoutParams(lp);
            lp = (ViewGroup.MarginLayoutParams) mToolbarTitle.getLayoutParams();
            lp.setMargins(0, 0, 0, 0);
            mToolbarTitle.setLayoutParams(lp);
        }
        adjust15sController();
    }

    private void adjust15sController(){ //調整快進快退15秒按鈕的位置
        UnitConverter converter = new UnitConverter(this);
        int width, groupMargin;
        width = mScreenW;

        if (delete.getVisibility() != View.GONE)
            width = width - (int) (delete.getWidth() * 2);
        if (download.getVisibility() != View.GONE)
            width = width - (int) (download.getWidth() * 2);

        groupMargin = (int) converter.convertPixelToDp(width/16);
        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) prev.getLayoutParams();
        lp.setMargins(0,0, groupMargin,0);
        prev.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) next.getLayoutParams();
        lp.setMargins(groupMargin,0,0,0);
        next.setLayoutParams(lp);

        groupMargin = (int) converter.convertPixelToDp(width/24);
        lp = (ViewGroup.MarginLayoutParams) prev15.getLayoutParams();
        lp.setMargins(0,0, groupMargin,0);
        prev15.setLayoutParams(lp);
        lp = (ViewGroup.MarginLayoutParams) next15.getLayoutParams();
        lp.setMargins(groupMargin,0,0,0);
        next15.setLayoutParams(lp);
    }

    public void changeVideoSize() {
        if(mediaPlayer == null)
            return;
        else{
            if (mediaPlayer.getVideoWidth() == 0 || mediaPlayer.getVideoHeight() == 0)
                return;
        }

        int videoWidth = mediaPlayer.getVideoWidth();
        int videoHeight = mediaPlayer.getVideoHeight();

        int maxWidth=0, maxHeight=0;
        maxWidth = mScreenW;
        maxHeight = mScreenH;

        if (videoWidth >= videoHeight){
            videoHeight = (int) (videoHeight * ((double) maxWidth / videoWidth));
            videoWidth = maxWidth;
        }
        else{
            videoWidth = (int) (videoWidth * ((double) maxHeight / videoHeight));
            videoHeight = maxHeight;
        }

        //無法直接設置視頻尺寸，將計算出的視頻尺寸設置到surfaceView 讓視頻自動填充。
        surfaceView.setLayoutParams(new RelativeLayout.LayoutParams(videoWidth, videoHeight));
        video_image.setLayoutParams(new RelativeLayout.LayoutParams(videoWidth, videoHeight));
    }

    @Override
    public void onWindowFocusChanged (boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
        int difference = 0;
        int rotation = getWindowManager().getDefaultDisplay().getRotation();
        switch (rotation) {
            case Surface.ROTATION_0:
                if(hasFocus){
                    System.out.println("onWindowFocusChanged");
                    difference = FrameworkConstant.mPortraitScreenHeight - control_layout.getBottom();
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustPortraitUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_90:
                if(hasFocus){   //由於需要判斷控件寬度，所以此處focus後要再調整一次
                    difference = FrameworkConstant.mPortraitScreenHeight - control_layout.getRight();
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustLandscapeUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_270:
                if(hasFocus){   //由於需要判斷控件寬度，所以此處focus後要再調整一次
                    difference = control_layout.getLeft() - Navigation_bar_height;
                    if (difference > Navigation_bar_height / 2)
                        Navigation_bar_height = 0;

                    adjustLandscapeUi();
                    adjust15sController();
                }
                break;
            case Surface.ROTATION_180:
                break;
        }
    }
}