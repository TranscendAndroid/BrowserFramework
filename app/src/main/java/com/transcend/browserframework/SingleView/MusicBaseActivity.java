package com.transcend.browserframework.SingleView;

import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

/**
 * Created by mike_chen on 2018/1/29.
 */

public class MusicBaseActivity extends AppCompatActivity {

    private Toolbar mToolbar;
    private TextView mToolbarTitle;

    private TextView album, artist; //專輯；演奏家
    private SeekBar mSeekbar;
    private TextView current_time, duration;    //現在時間；總時間
    private ImageView play, prev, next;
    private ImageView repeat, shuffle;  //重複播放；隨機播放
    private ImageView album_image;

    //TODO 改為abstract
    private OnMusicViewControllerListener musicControllerListener;
    public interface OnMusicViewControllerListener{
        void playButtonPress();
        void nextButtonPress();
        void previousButtonPress();
        void repeatButtonPress();
        void shuffleButtonPress();
    }
    public void setOnMusicViewControllerListener(OnMusicViewControllerListener listener){
        musicControllerListener = listener;
    }

    protected void setMusicContentView() {
        //將UI的處理規劃另外作，再回傳給Activity
        LayoutInflater mInflater = LayoutInflater.from(this);
        View view = UiHelper.setMusicUiDimensioning(this, mInflater);
        setContentView(view);

        iniView();
        iniToolbar();

        album_image.setImageResource(R.drawable.img_singleview_music);

        //System bar 透明化
        UiHelper.setSystemBarTranslucent(this);
    }

    private void iniView(){
        album = (TextView) findViewById(R.id.music_album);
        artist = (TextView) findViewById(R.id.music_artist);

        mSeekbar = (SeekBar) findViewById(R.id.music_seekbar);

        current_time = (TextView) findViewById(R.id.music_current_time);
        duration = (TextView) findViewById(R.id.music_duration);

        play = (ImageView) findViewById(R.id.music_play);
        prev = (ImageView) findViewById(R.id.music_previous);
        next = (ImageView) findViewById(R.id.music_next);

        repeat = (ImageView) findViewById(R.id.music_repeat);
        shuffle = (ImageView) findViewById(R.id.music_shuffle);

        album_image = (ImageView) findViewById(R.id.music_image);

        if (UiHelper.isPad(this))
            album_image.setScaleType(ImageView.ScaleType.CENTER);
        else
            album_image.setScaleType(ImageView.ScaleType.FIT_CENTER);

        play.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicControllerListener.playButtonPress();
            }
        });

        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicControllerListener.nextButtonPress();
            }
        });

        prev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicControllerListener.previousButtonPress();
            }
        });

        shuffle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicControllerListener.shuffleButtonPress();
            }
        });

        repeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                musicControllerListener.repeatButtonPress();
            }
        });
    }

    private void iniToolbar(){
        mToolbar = (Toolbar) findViewById(R.id.music_header_bar);
        mToolbarTitle = (TextView) findViewById(R.id.music_toolbar_title);
        UnitConverter converter = new UnitConverter(this);
        mToolbarTitle.setTextSize(converter.convertPtToSp(34));
        mToolbar.setNavigationIcon(R.drawable.ic_back);
        setSupportActionBar(mToolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);
    }

    protected void setPlayBtnImageResource(int resId){
        play.setImageResource(resId);
    }

    protected void setCurrentTimeText(String text){
        current_time.setText(text);
    }

    protected void setDurationText(String text){
        duration.setText(text);
    }

    protected void setShuffleImageResource(int resId){
        shuffle.setImageResource(resId);
    }

    protected void setRepeatImageResource(int resId){
        repeat.setImageResource(resId);
    }

    protected ImageView getMusicAlbumImageView(){
        return album_image;
    }

    protected void setAlbumText(String text){
        album.setText(text);
    }

    protected void setArtistText(String text){
        artist.setText(text);
    }

    protected SeekBar getSeekbar(){
        return mSeekbar;
    }

    protected Toolbar getToolbar(){
        return mToolbar;
    }

    protected void setToolbarTitle(String text){
        mToolbarTitle.setText(text);
    }

    protected void enablePrevBtn(boolean enable){
        prev.setEnabled(enable);
        if (enable)
            prev.setImageResource(R.drawable.ic_prev_large);
        else
            prev.setImageResource(R.drawable.ic_singleview_playback_grey);
    }

    protected void enableNextBtn(boolean enable){
        next.setEnabled(enable);
        if (enable)
            next.setImageResource(R.drawable.ic_next_large);
        else
            next.setImageResource(R.drawable.ic_singleview_playnext_grey);
    }

    protected void setShuffleButtonVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE){
            shuffle.setVisibility(visibility);
        }
    }

    protected void setRepeatButtonVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE){
            repeat.setVisibility(visibility);
        }
    }

    protected void setAlbumTextVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            album.setVisibility(visibility);
    }

    protected void setArtistTextVisibility(int visibility){
        if(visibility == View.GONE || visibility == View.VISIBLE || visibility == View.INVISIBLE)
            artist.setVisibility(visibility);
    }
}
