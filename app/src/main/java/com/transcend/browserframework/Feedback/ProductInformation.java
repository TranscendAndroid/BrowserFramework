package com.transcend.browserframework.Feedback;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.transcend.browserframework.Browser.ToolbarController;
import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UiHelper;
import com.transcend.browserframework.Utils.UnitConverter;

import java.util.Locale;

public class ProductInformation extends AppCompatActivity {

    TextView intro;
    ImageView img;
    Button confirm;

    final static String DP = "Car Video Recorders";
    final static String DPB = "Body Camera";
    String SNUri;

    ToolbarController mToolbarController = new ToolbarController(this);

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_product_sn_info);

        initToolbar();

        init();

        uiAdjust();

        setContent();
    }

    private void initToolbar(){
        mToolbarController.toolbar_titleMode();
        mToolbarController.setToolbarTitle(getResources().getString(R.string.framework_product_intro_title));
    }

    private void init(){
        intro = findViewById(R.id.product_introduction);
        img = findViewById(R.id.product_pic);

        confirm = findViewById(R.id.button_confirm);
        confirm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void uiAdjust(){
        UnitConverter converter = new UnitConverter(this);

        ViewGroup.MarginLayoutParams lp = (ViewGroup.MarginLayoutParams) intro.getLayoutParams();
        lp.setMargins(0, (int) converter.convertPixelToDp(114), 0, 0);
        intro.setLayoutParams(lp);

        lp = (ViewGroup.MarginLayoutParams) img.getLayoutParams();
        lp.setMargins(0, (int) converter.convertPixelToDp(78), 0, 0);
        img.setLayoutParams(lp);

        intro.setTextSize(converter.convertPtToSp(32));
        intro.setTextColor(getResources().getColor(R.color.c_02));
    }

    private void setContent(){
        String product = getIntent().getStringExtra("parse_tag");

        String text = "";
        Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_feedback_product_sn_dpb);

        if (product.equals(DPB)){
            text = getString(R.string.framework_product_intro_dpb);
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_feedback_product_sn_dpb);
        }
        else if(product.equals(DP)){
            text = getString(R.string.framework_product_intro_dp);
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.img_feedback_product_sn_dp);
        }
        else{
            SNUri = createSNUri();
            try {
                Intent intent = new Intent();
                intent.setAction(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(SNUri));
                startActivity(intent);
            }catch(ActivityNotFoundException a){
                a.getMessage();
            }
            this.finish();
        }

        if (UiHelper.isPad(this)){
            int width = bitmap.getWidth();
            int height = bitmap.getHeight();

            float scaleWidth = 1.5f;
            float scaleHeight = 1.5f;
            // create a matrix for the manipulation
            Matrix matrix = new Matrix();
            // resize the Bitmap
            matrix.postScale(scaleWidth, scaleHeight);
            // if you want to rotate the Bitmap
            // matrix.postRotate(45);
            // recreate the new Bitmap
            Bitmap resizedBitmap = Bitmap.createBitmap(bitmap, 0, 0, width,height, matrix, true);
            // make a Drawable from Bitmap to allow to set the Bitmap
            // to the ImageView, ImageButton or what ever
            img.setImageBitmap(resizedBitmap);
        }
        else
            img.setImageBitmap(bitmap);

        intro.setText(text);
    }

    private String createSNUri(){
        String localCode = "us";

        String currentLanguage = Locale.getDefault().getLanguage().toLowerCase().trim();

        if (Locale.getDefault().getLanguage() != null){
            if (currentLanguage.equals("zh")){
                if (Locale.getDefault().getCountry().toLowerCase().trim().equals("cn"))
                    localCode = "cn";
                else
                    localCode = "tw";
            }
            else if (currentLanguage.equals("de")){
                localCode = "de";
            }
            else if (currentLanguage.equals("es")){
                localCode = "es";
            }
            else if (currentLanguage.equals("fr")){
                localCode = "fr";
            }
            else if (currentLanguage.equals("it")){
                localCode = "it";
            }
            else if (currentLanguage.equals("ja")){
                localCode = "jp";
            }
            else if (currentLanguage.equals("ko")){
                localCode = "kr";
            }
            else if (currentLanguage.equals("pt")){
                localCode = "pt";
            }
            else if (currentLanguage.equals("ru")){
                localCode = "ru";
            }
            else if (currentLanguage.equals("th")){
                localCode = "th";
            }
            else if (currentLanguage.equals("tr")){
                localCode = "tr";
            }
        }

        return "https://"+ localCode +".transcend-info.com/supports/find_sn.aspx?trgForm=rmadT_0__ItemID&trgForm2=TrmadT_0";
    }
}