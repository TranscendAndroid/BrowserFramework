package com.transcend.browserframework.Feedback;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.InputType;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.transcend.browserframework.BuildConfig;
import com.transcend.browserframework.PopDialog.PopDialog;
import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UnitConverter;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;
import org.xmlpull.v1.XmlPullParserFactory;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.net.ssl.HttpsURLConnection;

import jcifs.util.Base64;

public class FeedbackFragment_v4 extends Fragment {

    private static final String TAG = FeedbackFragment_v4.class.getSimpleName();

    private Context mContext;

    //2018/08/23 新服務網址
    private static final String CATEGORY_LIST_URL = "https://www.transcend-info.com/Service/SMSService.svc/webssl/GetSrvCategoryList";
    private static final String FEEDBACK_URL = "https://www.transcend-info.com/Service/SMSService.svc/webssl/ServiceMailCaseAdd";
//    private static final String FEEDBACK_URL = "http://10.13.5.10/Service/SMSService.svc/web/ServiceMailCaseAdd";

    private static final String KEY_SERVICE_TYPE = "service_type";          //Server 給的值，可從XML解析得到
    private static final String KEY_SERVICE_CATEGORY = "service_category";//Server 給的值，可從XML解析得到

    private static final int ID_ERROR_HANDLING = -1;
    private static final int ID_GET_CATEGORY_LIST = 0;
    private static final int ID_SEND_FEEDBACK = 1;

    private static String PRODUCT_NAME = "";
    private static final String REGION = "Taiwan";
    private static String AppVersion = BuildConfig.VERSION_NAME;

    private static String XML_PARSER_TAG = "";
    private static final String XML_PARSER_NAME = "CName";
    private static final String XML_PARSER_TYPE = "CType";
    private static final String XML_PARSER_CATEGORY = "WebCatNo";

    private TextView text_name, text_email, text_sn, text_message;
    private EditText input_name, input_email, input_sn, input_message;
    private ImageView sn_info;
    private Button send;
    private View mProgressBar;

    public static final String FEEDBACK_NAME = "feedback_name";
    public static final String FEEDBACK_MAIL = "feedback_mail";
    public static final String FEEDBACK_SN = "feedback_sn";

    public static FeedbackFragment_v4 newInstance(String productName, String xmlParseTag, String appVersion){
        FeedbackFragment_v4 f = new FeedbackFragment_v4();
        PRODUCT_NAME = productName;
        XML_PARSER_TAG = xmlParseTag;
        AppVersion = appVersion;
        return f;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = context;
    }

    private boolean checkContext(){
        if (mContext == null)
            mContext = getActivity();

        return mContext != null;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        RelativeLayout root = (RelativeLayout) inflater.inflate(R.layout.fragment_feedback, container, false);

        text_name = root.findViewById(R.id.text_name);
        text_email = root.findViewById(R.id.text_email);
        text_sn = root.findViewById(R.id.text_sn_number);
        text_message = root.findViewById(R.id.text_message);

        input_name = root.findViewById(R.id.input_name);
        input_email = root.findViewById(R.id.input_email);
        input_sn = root.findViewById(R.id.input_sn);
        input_message = root.findViewById(R.id.input_message);

        input_name.setOnFocusChangeListener(new MyFocusChangeListener(input_name));
        input_email.setOnFocusChangeListener(new MyFocusChangeListener(input_email));
        input_message.setOnFocusChangeListener(new MyFocusChangeListener(input_message));
        input_sn.setOnFocusChangeListener(new MyFocusChangeListener(input_sn));

        input_name.addTextChangedListener(new MyTextWatcher(input_name));
        input_email.addTextChangedListener(new MyTextWatcher(input_email));
        input_message.addTextChangedListener(new MyTextWatcher(input_message));
        input_sn.addTextChangedListener(new SerialNumberWatcher(input_sn));

        sn_info = root.findViewById(R.id.sn_info);
        sn_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkContext()) {
                    Intent intent = new Intent(mContext, ProductInformation.class);
                    intent.putExtra("parse_tag", XML_PARSER_TAG);
                    mContext.startActivity(intent);
                }
            }
        });

        //檢查bundle是否為null
        Bundle bundle = getArguments();
        if (bundle != null) {
            String name, email, sn_text;
            name = getArguments().getString(FEEDBACK_NAME);
            email = getArguments().getString(FEEDBACK_MAIL);
            sn_text = getArguments().getString(FEEDBACK_SN);

            if ((name == null || name.length() == 0) && (email != null && email.contains("@"))) {
                name = email.split("@")[0];
            }
            input_name.setText(name);
            input_email.setText(email);
            input_sn.setText(sn_text);
            if (sn_text != null && sn_text.length() != 0) {
                if (isValidSerialNumber(false)) {
                    input_sn.setEnabled(false);
                    if (checkContext())
                        input_sn.setTextColor(mContext.getResources().getColor(R.color.c_04));
                    sn_info.setVisibility(View.GONE);
                }
            }
        }

        send = root.findViewById(R.id.btn_send);
        send.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendFeedback();
            }
        });
        EnableSendBtn(false);

        mProgressBar = root.findViewById(R.id.feedback_progress_bar);

        adjustUI();

        return root;
    }

    private void adjustUI(){
        if (!checkContext())
            return;

        UnitConverter converter = new UnitConverter(mContext);
        text_name.getLayoutParams().height = (int) converter.convertPixelToDp(98);
        text_email.getLayoutParams().height = (int) converter.convertPixelToDp(98);
        text_sn.getLayoutParams().height = (int) converter.convertPixelToDp(98);
        text_message.getLayoutParams().height = (int) converter.convertPixelToDp(98);

        text_name.setTextColor(mContext.getResources().getColor(R.color.c_02));
        text_email.setTextColor(mContext.getResources().getColor(R.color.c_02));
        text_sn.setTextColor(mContext.getResources().getColor(R.color.c_02));
        text_message.setTextColor(mContext.getResources().getColor(R.color.c_02));

        text_name.setTextSize(converter.convertPtToSp(28));
        text_email.setTextSize( converter.convertPtToSp(28));
        text_sn.setTextSize(converter.convertPtToSp(28));
        text_message.setTextSize(converter.convertPtToSp(28));

        input_name.getLayoutParams().height = (int) converter.convertPixelToDp(80);
        input_email.getLayoutParams().height = (int) converter.convertPixelToDp(80);
        input_sn.getLayoutParams().height = (int) converter.convertPixelToDp(80);

        int px38 = (int) converter.convertPixelToDp(38);
        int px12 = (int) converter.convertPixelToDp(12);
        text_name.setPadding(px38, 0, 0, px12);
        text_email.setPadding(px38, 0, 0, px12);
        text_sn.setPadding(px38, 0, 0, px12);
        text_message.setPadding(px38, 0, 0, px12);
        input_name.setPadding(px38, 0, px38, 0);
        input_email.setPadding(px38, 0, px38, 0);
        input_sn.setPadding(px38, 0, px38, 0);
        input_message.setPadding(px38, (int) converter.convertPixelToDp(24), px38, 0);

        input_name.setTextSize(converter.convertPtToSp(36));
        input_email.setTextSize(converter.convertPtToSp(36));
        input_sn.setTextSize(converter.convertPtToSp(36));
        input_message.setTextSize(converter.convertPtToSp(36));

        sn_info.setPadding(px12, 0, px12, (int) converter.convertPixelToDp(5));
    }

    public void sendFeedback() {
        if (isInputValid()) {
            mProgressBar.setVisibility(View.VISIBLE);
            sendRequest(configurePostRequest(CATEGORY_LIST_URL), null, ID_GET_CATEGORY_LIST);
        }
    }

    private void EnableSendBtn(boolean enable){
        send.setEnabled(enable);
        if (checkContext())
            send.setBackground(mContext.getResources().getDrawable(R.drawable.button_bottom_red));
        else
            send.setBackgroundColor(mContext.getResources().getColor(R.color.c_04));
    }

    private boolean isInputValid() {
        return isValidName() && isValidEmail() && isValidSerialNumber(true) && isValidMessage();
    }

    private boolean isValidName() {
        String name = input_name.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidEmail() {
        String email = input_email.getText().toString().trim();
        boolean isValid = !TextUtils.isEmpty(email) && android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();

        if (!isValid) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidMessage() {
        String message = input_message.getText().toString().trim();

        if (TextUtils.isEmpty(message)) {
            return false;
        } else {
            return true;
        }
    }

    private boolean isValidSerialNumber(boolean showDialog) {
        String sn = input_sn.getText().toString().trim();
        String original_sn = sn.replaceAll("-", "");

        if (original_sn.length() == 0)
            return true;

        if(original_sn.length() != 10) {
            if (showDialog) {
                if (getActivity() != null) {
                    PopDialog dialog = new PopDialog(getActivity());
                    dialog.setTitle(getString(R.string.framework_error));
                    dialog.setMessage(getString(R.string.framework_invalid_sn));
                    Button confirm = new Button(getActivity());
                    confirm.setText(getString(R.string.framework_confirm));
                    dialog.setPositiveBtn(confirm);
                    dialog.buildNormalDialog();
                    if (getActivity() != null && !getActivity().isFinishing())
                        dialog.show();
                }
            }
            return false;
        } else{
            return true;
        }
    }

    private HttpURLConnection configurePostRequest(String requestUrl) {
        HttpURLConnection conn = null;
        try {
            URL url = new URL(requestUrl);
            conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("POST");
            conn.setRequestProperty("content-type", "application/json");
            conn.setDoOutput(true);
            conn.setReadTimeout(10000);
            conn.setConnectTimeout(10000);
        } catch (Exception e) {
            Log.d(TAG, "HttpURLConnection========================================================================");
            e.printStackTrace();
            doErrorHandling();
            Log.d(TAG, "HttpURLConnection========================================================================");
        }
        return conn;
    }

    private void doErrorHandling() {
        Message msg = new Message();
        msg.what = ID_ERROR_HANDLING;
        mHandler.sendMessage(msg);
    }

    private void sendRequest(final HttpURLConnection connection, final String jsonData, final int messageId) {
        if (connection == null) {
            return;
        }

        new Thread(new Runnable() {

            @Override
            public void run() {
                Message msg = new Message();
                try {
                    OutputStream out = connection.getOutputStream();
                    if (jsonData != null) {
                        out.write(jsonData.getBytes());
                    }
                    out.flush();
                    out.close();

                    int responseCode = connection.getResponseCode();
                    Log.d(TAG, "param: " + jsonData);
                    Log.d(TAG, "responseCode: " + responseCode);

                    if (responseCode == HttpsURLConnection.HTTP_OK) {
                        String result = getResponseResult(connection);
                        Log.d(TAG, "response result: " + result);

                        if (messageId == ID_GET_CATEGORY_LIST && !parserCategoryData(msg, result)) {
                            doErrorHandling();
                            return;
                        }

                        msg.what = messageId;
                        mHandler.sendMessage(msg);
                    }

                } catch (IOException e) {
                    Log.d(TAG, "IOException========================================================================");
                    e.printStackTrace();
                    doErrorHandling();
                    Log.d(TAG, "IOException========================================================================");
                }
            }

        }).start();

    }

    private boolean parserCategoryData(Message msg, String responseData) {
        //Parser response data to get two values: CType, WebCatNo
        Map<String, String> mKeys = new HashMap<>();
        mKeys.put(XML_PARSER_TYPE, KEY_SERVICE_TYPE);
        mKeys.put(XML_PARSER_CATEGORY, KEY_SERVICE_CATEGORY);

        Bundle data = new Bundle();
        if (responseData != null) {
            XmlPullParserFactory factory;

            try {
                factory = XmlPullParserFactory.newInstance();
                factory.setNamespaceAware(true);
                XmlPullParser parser = factory.newPullParser();
                parser.setInput(new StringReader(responseData));
                int eventType = parser.getEventType();
                String curTagName = null;
                boolean startAdd = false;

                do {
                    String tagName = parser.getName();
                    if (eventType == XmlPullParser.START_TAG) {
                        curTagName = tagName;
                    } else if (eventType == XmlPullParser.TEXT) {
                        if (curTagName != null) {
                            String text = parser.getText();
                            if (startAdd) {
                                for (String key : mKeys.keySet()) {
                                    if (curTagName.equals(key)) {
                                        Log.d(TAG, "Test: " + key + ", " + mKeys.get(key) + ", " + text);
                                        data.putString(mKeys.get(key), text);
                                        mKeys.remove(key);
                                        break;
                                    }
                                }

                                if (mKeys.size() == 0)
                                    break;
                            } else {
                                startAdd = XML_PARSER_NAME.equals(curTagName) && XML_PARSER_TAG.equals(text);
                            }
                        }
                    } else if (eventType == XmlPullParser.END_TAG) {
                        curTagName = null;
                    }
                    eventType = parser.next();
                } while (eventType != XmlPullParser.END_DOCUMENT);

            } catch (XmlPullParserException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            //Use the default value to send feedback
            data.putString(KEY_SERVICE_TYPE, String.valueOf(15));
            data.putString(KEY_SERVICE_CATEGORY, String.valueOf(384));
            mKeys.clear();
        }

        msg.setData(data);
        return mKeys.size() == 0;
    }

    private String getResponseResult(HttpURLConnection connection) throws IOException {
        BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        String line = in.readLine();

        Log.d(TAG, "get category list result: " + line);
        in.close();
        return line;
    }

    private Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);

            switch (msg.what) {
                case ID_GET_CATEGORY_LIST:
                    String feedbackData = getFeedbackData(msg);
                    if (feedbackData != null) {
                        sendRequest(configurePostRequest(FEEDBACK_URL), feedbackData, ID_SEND_FEEDBACK);
                    }
                    break;
                case ID_SEND_FEEDBACK:
                    mProgressBar.setVisibility(View.INVISIBLE);
                    if (checkContext())
                        Toast.makeText(mContext, R.string.framework_feedback_send_success, Toast.LENGTH_SHORT).show();
                    input_message.setText("");
                    break;
                case ID_ERROR_HANDLING:
                    mProgressBar.setVisibility(View.INVISIBLE);
                    if (checkContext())
                        Toast.makeText(mContext, R.string.framework_feedback_send_error, Toast.LENGTH_LONG).show();
                    break;
            }
        }

        private String getFeedbackData(Message msg) {
            String jsonData = null;
            if (msg.getData() != null) {
                String srvType = msg.getData().getString(KEY_SERVICE_TYPE);
                String srvCategory = msg.getData().getString(KEY_SERVICE_CATEGORY);
                String platformInfo = "App v" + AppVersion + " OS version: " + Build.VERSION.SDK_INT +
                        " device name: " + getAndroidDeviceName();

                jsonData = "{\"DataModel\":{\"CustName\":\"" + input_name.getText().toString() + "\"" +
                        ",\"CustEmail\":\"" + input_email.getText().toString() + "\"" +
                        ",\"Region\":\"" + REGION + "\"" +
                        ",\"ISOCode\":\"TW\"" +
                        ",\"Request\":\"" + platformInfo + "\"" +
                        ",\"SrvType\":\"" + srvType + "\"" +
                        ",\"SrvCategory\":\"" + srvCategory + "\"" +
                        ",\"ProductName \":\"" + PRODUCT_NAME + "\"" +
                        ",\"SerialNo\":\"" + input_sn.getText().toString().replaceAll("-", "").toUpperCase() + "\"" +
                        ",\"LocalProb\":\"" + Base64.encode(input_message.getText().toString().getBytes()) + "\"}}";
            }
            return jsonData;
        }
    };

    private String getAndroidDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer)) {
            return capitalize(model);
        } else {
            return capitalize(manufacturer) + " " + model;
        }
    }

    private String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    private class MyFocusChangeListener implements View.OnFocusChangeListener {

        private View view;

        private MyFocusChangeListener(View view) {
            this.view = view;
        }

        @Override
        public void onFocusChange(View v, boolean hasFocus) {
            if (!hasFocus) {
                if (view.getId() == R.id.input_name) {
                    isValidName();
                }
                else if (view.getId() == R.id.input_email){
                    isValidEmail();
                }
                else if (view.getId() == R.id.input_message){
                    isValidMessage();
                }
                else if (view.getId() == R.id.input_sn){
                    isValidSerialNumber(true);
                }
            }
        }
    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
        }

        public void afterTextChanged(Editable editable) {
            if (send == null)
                return;

            if (isValidName() && isValidEmail() && isValidSerialNumber(false) && isValidMessage())
                EnableSendBtn(true);
            else
                EnableSendBtn(false);
        }
    }

    private class SerialNumberWatcher implements TextWatcher {
        private EditText view;

        private SerialNumberWatcher(EditText view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

        public void onTextChanged(CharSequence s, int start, int before, int count) {
            view.removeTextChangedListener(this);

            if (s.length() > 11)
                view.setText(s.subSequence(0, 11));

            if (s.length() == 0)
                view.setInputType(InputType.TYPE_CLASS_TEXT);
            else
                view.setInputType(InputType.TYPE_CLASS_NUMBER);

            if (s.length() > 6){
                if (before > 0 && s.charAt(s.length() - 1) == '-'){
                    view.setText(s.toString().replaceAll("-", ""));
                }

                if (before == 0 && !s.toString().contains("-")) {
                    StringBuffer sb = new StringBuffer(s);
                    sb.insert(6, "-");
                    view.setText(sb);
                }
            }

            int pos = view.getText().toString().length();
            view.setSelection(pos);
            view.addTextChangedListener(this);
        }

        public void afterTextChanged(Editable editable) {
            if (send == null)
                return;

            if (isValidName() && isValidEmail() && isValidSerialNumber(false) && isValidMessage())
                EnableSendBtn(true);
            else
                EnableSendBtn(false);
        }
    }

    //serial number: 10位純數字
    public void setSerialNumber(String serialNumber){
        input_sn.setText(serialNumber);
        if (isValidSerialNumber(false)) {
            input_sn.setEnabled(false);
            if (checkContext())
                input_sn.setTextColor(mContext.getResources().getColor(R.color.c_04));
            sn_info.setVisibility(View.GONE);
        }
    }

    public void setAppVersion(String version){
        AppVersion = version;
    }
}