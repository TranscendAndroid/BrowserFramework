package com.transcend.browserframework;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.SpinnerAdapter;

import com.transcend.browserframework.Browser.DropDownAdapter;
import com.transcend.browserframework.Browser.ToolbarController;

public abstract class ToolbarActivity extends AppCompatActivity implements
        ToolbarController.OnToolbarItemClick,
        DropDownAdapter.OnDropdownItemSelectedListener {

    protected abstract FrameworkBuilder onCreateBuilder();

    private ToolbarController mToolbarController;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FrameworkBuilder builder = onCreateBuilder();

        //Init transcend drawer controller
        initDrawer(builder);

        //Init transcend toolbar controller
        initToolbar(builder);
    }

    protected void initDrawer(FrameworkBuilder builder) {
        if (builder == null)
            return;

        //In new drawer framework, for the view only has toolbar and without drawer
        //we don't need to init drawer controller at first, and call setContentView directly
        setContentView(builder.getLayoutID());
    }

    //初始化Toolbar
    protected void initToolbar(FrameworkBuilder builder) {
        if (builder == null)
            return;

        mToolbarController = new ToolbarController(this);
        mToolbarController.setToolbarTitle(builder.getTitle());
        mToolbarController.setOnToolbarItemListener(this);
        mToolbarController.setOnDropdownItemSelectedListener(this);

        updateToolbarMode(builder.getToolbarMode());
    }

    //更新toolbar模式
    protected void updateToolbarMode(ToolbarController.ToolbarMode mode) {
        if (mToolbarController == null)
            return;

        switch (mode) {
            case NORMAL:
                mToolbarController.toolbar_normalMode();
                break;
            case NORMAL_DROPDOWN:
                mToolbarController.toolbar_normalDropDownMode();
                break;
            case BACK:
                mToolbarController.toolbar_backMode();
                break;
            case BACK_DROPDOWN:
                mToolbarController.toolbar_backDropDownMode();
                break;
            case BACK_LEFT:
                mToolbarController.toolbar_backLeftMode();
                break;
            case CLOSE:
                mToolbarController.toolbar_closeMode();
                break;
            case CLOSE_DROPDOWN:
                mToolbarController.toolbar_closeDropDownMode();
                break;
            case BROWSER:
                mToolbarController.toolbar_browserMode();
                break;
            case SEARCH:
                mToolbarController.toolbar_searchMode();
                break;
            case TITLE:
                mToolbarController.toolbar_titleMode();
                break;
            default:
                mToolbarController.toolbar_normalMode();
                break;
        }
    }

    //給予權限來新增自己需求的adapter
    final protected void setDropDownAdapter(SpinnerAdapter adapter) {
        mToolbarController.setDropDownAdapter(adapter);
    }

    //開啟/關閉dropdown功能
    protected void enableDropDown(boolean enable) {
        mToolbarController.enableDropDown(enable);
    }

    //設定Toolbar標題
    protected void setToolbarTitle(String text){
        mToolbarController.setToolbarTitle(text);
    }

    protected String getToolbarTitle(){
        return mToolbarController.getToolbarTitle();
    }

    protected String getDropDownTopText(){
        return mToolbarController.getDropDownTopText();
    }

    public void setTitlePendingWithMenuItems(int items){
        mToolbarController.setTitlePendingWithMenuItems(items);
    }

    protected void setTitlePending(int left, int right){
        mToolbarController.setTitlePending(left, right);
    }

    protected void setToolbarVisibility(int visibility){
        mToolbarController.setToolbarVisibility(visibility);
    }

    //制定Dropdown一開始顯示的字串
    protected void setMainPageTitle(String text){
        mToolbarController.setMainPageTitle(text);
    }

    //更新Dropdown內容，需為路徑模式(/storage/DCIM)
    protected void updateDropDownList(String path){
        mToolbarController.updateDropDownList(path);
    }

    protected String getPathAtPosition(int position){
        return mToolbarController.getPathAtPosition(position);
    }

    protected ToolbarController.ToolbarMode getCurrentToolbarMode(){
        return mToolbarController.getCurrentToolbarMode();
    }

    /**
     * Transcend Toolbar Callback
     */
    @Override
    public void OnToggleClick() {
        finish();   //wait for override
    }

    @Override
    public void OnSearchModeSubmitClick(String searchText) {
        //Do Nothing, wait for override
    }

    @Override
    public void OnSearchModeEditTextChange(String text) {
        //Do Nothing, wait for override
    }

    @Override
    public void OnSearchModeStart() {
        //Do Nothing, wait for override
    }

    @Override
    public void OnSearchModeExit() {
        //Do Nothing, wait for override
    }

    @Override
    public void onDropdownItemSelected(int position) {
        //Do Nothing, wait for override
    }
}
