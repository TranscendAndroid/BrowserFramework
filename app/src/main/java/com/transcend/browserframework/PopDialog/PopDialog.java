package com.transcend.browserframework.PopDialog;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import com.transcend.browserframework.R;
import com.transcend.browserframework.Utils.UnitConverter;

import java.lang.ref.WeakReference;

/**
 * Created by mike_chen on 2018/3/2.
 */

public class PopDialog {

    private String TAG = PopDialog.class.getSimpleName();
    private WeakReference<Context> mContext;

    private String mTitle, mMessage;
    private Button okBtn, cancelBtn, neutralBtn;

    private AlertDialog alertDialog;
    private ProgressDialog progressDialog;
    private int progressStatus;
    private int progressMax = 0;

    private LinearLayout layout;

    public PopDialog(Context context){
        mContext = new WeakReference<>(context);
    }

    public void buildNormalDialog(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        alertDialog = new AlertDialog.Builder(context)
                .setTitle(mTitle)
                .setMessage(mMessage)
                .setCancelable(false)
                .create();
        if (okBtn != null){
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, okBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okBtn.performClick();
                }
            });
        }
        if (cancelBtn != null){
            alertDialog.setButton(Dialog.BUTTON_NEGATIVE, cancelBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelBtn.performClick();
                }
            });
        }
        if (neutralBtn != null){
            alertDialog.setButton(Dialog.BUTTON_NEUTRAL, neutralBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    neutralBtn.performClick();
                }
            });
        }
    }

    public void buildWaitingDialog(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        progressDialog = ProgressDialog.show(context, mTitle, mMessage,true);
        Drawable drawable = new ProgressBar(context).getIndeterminateDrawable().mutate();
        drawable.setColorFilter(ContextCompat.getColor(context, R.color.c_06),
                PorterDuff.Mode.SRC_IN);
        progressDialog.setIndeterminateDrawable(drawable);
    }

    public void buildProgressDialog(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        progressDialog = new ProgressDialog(context);
        progressDialog.setTitle(mTitle);
        progressDialog.setMessage(mMessage);
        progressDialog.setCancelable(false);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setProgressDrawable(context.getResources().getDrawable(R.drawable.progressbar_style));
        progressDialog.setProgress(0);
        progressDialog.setMax(progressMax);
        if (cancelBtn != null) {
            progressDialog.setButton(Dialog.BUTTON_NEGATIVE, cancelBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelBtn.performClick();
                }
            });
        }
        progressStatus = 0;
    }

    public void buildEditTextDialog(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        alertDialog = new AlertDialog.Builder(context)
                .setTitle(mTitle)
                .setCancelable(false)
                .setView(layout)
                .create();
        if (okBtn != null){
            alertDialog.setButton(Dialog.BUTTON_POSITIVE, okBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    okBtn.performClick();
                }
            });
        }
        if (cancelBtn != null){
            alertDialog.setButton(Dialog.BUTTON_NEGATIVE, cancelBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    cancelBtn.performClick();
                }
            });
        }
        if (neutralBtn != null){
            alertDialog.setButton(Dialog.BUTTON_NEUTRAL, neutralBtn.getText(), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    neutralBtn.performClick();
                }
            });
        }
    }

    public void setProgressStatus(int progress){
        progressStatus = progress;
        progressDialog.setProgress(progressStatus);
    }

    public void setProgressMax(int max){
        progressMax = max;
    }

    public void setTitle(String title){
        mTitle = title;
    }

    public void setMessage(String message){
        mMessage = message;
    }

    public void setPositiveBtn(Button positive){
        okBtn = positive;
    }

    public void setNeutralBtn(Button neutral){
        neutralBtn = neutral;
    }

    public void setNegativeBtn(Button negative){
        cancelBtn = negative;
    }

    public void addCustomView(View t1){
        if (layout==null)
            initLinearLayout();

        layout.addView(t1);
    }

    private void initLinearLayout(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        UnitConverter format = new UnitConverter(context);
        layout = new LinearLayout(context);
        layout.setOrientation(LinearLayout.VERTICAL);
        int px48 = (int) format.convertPixelToDp(48);
        layout.setPadding(px48, 25, px48, 15);
    }

    public void show(){
        Context context = mContext.get();
        if(context != null){//判斷有無被系統GC
            context = mContext.get();
            //可以執行到這，就表示 context 還未被系統回收，可繼續做接下來的任務
        }
        else
            return;

        if (alertDialog != null) {
            alertDialog.show();

            //for negative side button
            if (cancelBtn != null)
                alertDialog.getButton(Dialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.c_06));
            //for positive side button
            if (okBtn != null)
                alertDialog.getButton(Dialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.c_06));
            //for neutral side button
            if (neutralBtn != null)
                alertDialog.getButton(Dialog.BUTTON_NEUTRAL).setTextColor(context.getResources().getColor(R.color.c_06));
        }
        if (progressDialog != null) {
            progressDialog.show();
            if (cancelBtn != null)
                progressDialog.getButton(Dialog.BUTTON_NEGATIVE).setTextColor(context.getResources().getColor(R.color.c_06));
            if (okBtn != null)
                progressDialog.getButton(Dialog.BUTTON_POSITIVE).setTextColor(context.getResources().getColor(R.color.c_06));
            if (neutralBtn != null)
                progressDialog.getButton(Dialog.BUTTON_NEUTRAL).setTextColor(context.getResources().getColor(R.color.c_06));
        }
    }

    public void dismiss(){
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}